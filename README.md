# Lumbatech Website

This is the repo for the lumbatech website.

### Getting Started
-------------------
This repo require

1. [npm] - The package manager for javascript.

2. [grunt] - The JavaScript Task Runner

3. [bower] - A package manager for the web

### Development
---------------
You need to run these command in you console:
```sh
$ npm install
```

```sh
$ bower update
```

```sh
$ grunt
```

Only run this on production stage:
```sh
$ grunt build
```

### Dashboard Access
-------------------

URL: http://localhost/admin

Username: admin

password: password

-------------------

**Happy coding!**

[npm]:https://www.npmjs.com/
[grunt]:http://gruntjs.com/
[bower]:http://bower.io/
