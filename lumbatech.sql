-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 13, 2015 at 06:19 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lumbatech`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `CategoryID` int(11) NOT NULL,
  `CategoryName` varchar(100) CHARACTER SET latin1 NOT NULL,
  `CategoryKeyname` varchar(100) COLLATE latin1_general_cs NOT NULL,
  `Parent` int(11) NOT NULL,
  `CategoryNote` varchar(100) COLLATE latin1_general_cs DEFAULT NULL,
  `Filename` varchar(100) COLLATE latin1_general_cs DEFAULT NULL,
  `Description` varchar(500) COLLATE latin1_general_cs DEFAULT NULL,
  `Position` smallint(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=130 DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`CategoryID`, `CategoryName`, `CategoryKeyname`, `Parent`, `CategoryNote`, `Filename`, `Description`, `Position`) VALUES
(111, 'Tombol Pemanggil', 'tombol-pemanggil', 0, NULL, '111-emnh76.png', '', 2),
(116, 'Produk Lain', 'produk-lain', 0, NULL, '116-o9kigc.png', '', 3),
(88, 'Kunci Pintu Digital', 'kunci-pintu-digital', 0, NULL, '88-32o13z.png', '', 1),
(87, 'Mesin Absensi', 'mesin-absensi', 0, NULL, '87-llvbqk.png', 'sadasdasdasd', 4);

-- --------------------------------------------------------

--
-- Table structure for table `msadmin`
--

DROP TABLE IF EXISTS `msadmin`;
CREATE TABLE IF NOT EXISTS `msadmin` (
  `admin_id` int(11) unsigned NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msadmin`
--

INSERT INTO `msadmin` (`admin_id`, `username`, `password`, `name`) VALUES
(1, 'admin', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Ryan Hidayat');

-- --------------------------------------------------------

--
-- Table structure for table `msbrand`
--

DROP TABLE IF EXISTS `msbrand`;
CREATE TABLE IF NOT EXISTS `msbrand` (
  `BrandID` int(11) unsigned NOT NULL,
  `BrandName` varchar(100) DEFAULT NULL,
  `BrandKeyname` varchar(100) DEFAULT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `BrandFilename` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msbrand`
--

INSERT INTO `msbrand` (`BrandID`, `BrandName`, `BrandKeyname`, `Description`, `BrandFilename`) VALUES
(1, 'Samsung', 'samsung', '', NULL),
(2, 'LG', 'lg', '', NULL),
(3, 'Toshiba', 'toshiba', '', NULL),
(5, 'samsung', 'samsung-2', '', NULL),
(6, 'samsung', 'samsung-3', '', NULL),
(7, 'mantap', 'mantap', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mspageinfo`
--

DROP TABLE IF EXISTS `mspageinfo`;
CREATE TABLE IF NOT EXISTS `mspageinfo` (
  `PageName` varchar(50) NOT NULL,
  `PageTitle` varchar(50) NOT NULL,
  `Content` text NOT NULL,
  `Filename` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mspageinfo`
--

INSERT INTO `mspageinfo` (`PageName`, `PageTitle`, `Content`, `Filename`) VALUES
('tentang', 'Tentang Kami', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod&nbsp;tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,&nbsp;quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo&nbsp;consequat. Duis aute irure dolor in reprehenderit in voluptate velit cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non&nbsp;proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod&nbsp;tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,&nbsp;quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo&nbsp;consequat. Duis aute irure dolor in reprehenderit in voluptate velit cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non&nbsp;proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'tentang-ue8cnv.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `msproduct`
--

DROP TABLE IF EXISTS `msproduct`;
CREATE TABLE IF NOT EXISTS `msproduct` (
  `ProductID` int(11) unsigned NOT NULL,
  `ProductName` varchar(200) DEFAULT NULL,
  `ProductDescription` text,
  `Filename` varchar(100) DEFAULT NULL,
  `BrandID` int(11) DEFAULT NULL,
  `CategoryID` int(11) DEFAULT NULL,
  `SEODescription` varchar(200) DEFAULT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT '1',
  `BrosurFilename` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msproduct`
--

INSERT INTO `msproduct` (`ProductID`, `ProductName`, `ProductDescription`, `Filename`, `BrandID`, `CategoryID`, `SEODescription`, `Status`, `BrosurFilename`) VALUES
(29, 'Product Name', '<p><img src="/media/data/03mm-ultra-thin-polycarbonate-materials-tpu-protection-shell-for-iphone-6-transparent-1.jpg" alt="" width="500" height="500" />asd sa</p>\n<p>asdasd</p>\n<p>asd</p>\n<p>dasdsad</p>', 'product-name-29.jpg', 1, 88, 'Seo description', 1, '29-b1v0gc.pdf'),
(30, 'Fingerprint Sensor', 'Finger Print Sensor', 'fingerprint-sensor-30.jpg', 2, 87, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mssliderimg`
--

DROP TABLE IF EXISTS `mssliderimg`;
CREATE TABLE IF NOT EXISTS `mssliderimg` (
  `SliderID` int(11) NOT NULL,
  `Filename` varchar(200) NOT NULL,
  `SmallFilename` varchar(200) DEFAULT NULL,
  `Link` varchar(200) DEFAULT NULL,
  `Position` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mssliderimg`
--

INSERT INTO `mssliderimg` (`SliderID`, `Filename`, `SmallFilename`, `Link`, `Position`) VALUES
(49, '49-4u5ksw.jpg', '49-4u5ksw.jpg', '', 3),
(50, '50-ccmcoh.jpg', '50-ccmcoh.jpg', '', 2),
(53, '53-yzqf67.png', '53-yzqf67.png', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trcategorybrand`
--

DROP TABLE IF EXISTS `trcategorybrand`;
CREATE TABLE IF NOT EXISTS `trcategorybrand` (
  `CategoryID` int(11) NOT NULL,
  `BrandID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trcategorybrand`
--

INSERT INTO `trcategorybrand` (`CategoryID`, `BrandID`) VALUES
(87, 1),
(87, 2),
(88, 1),
(88, 2),
(88, 3),
(111, 1),
(111, 3),
(117, 3),
(118, 3);

-- --------------------------------------------------------

--
-- Table structure for table `websitesetting`
--

DROP TABLE IF EXISTS `websitesetting`;
CREATE TABLE IF NOT EXISTS `websitesetting` (
  `SettingID` int(11) NOT NULL,
  `WebsiteName` varchar(100) NOT NULL,
  `WebsiteTitle` varchar(200) NOT NULL,
  `WebsiteDescription` text NOT NULL,
  `WebsiteContent` text NOT NULL,
  `WebsiteContact` text NOT NULL,
  `FacebookURL` varchar(150) NOT NULL,
  `InstagramURL` varchar(150) NOT NULL,
  `TwitterURL` varchar(150) NOT NULL,
  `GPlusURL` varchar(150) DEFAULT NULL,
  `YoutubeURL` varchar(150) DEFAULT NULL,
  `Address` text,
  `Phone` varchar(20) DEFAULT NULL,
  `Email` varchar(150) DEFAULT NULL,
  `WebsiteImage` varchar(25) DEFAULT NULL,
  `LargeLogo` varchar(20) NOT NULL,
  `SmallLogo` varchar(20) NOT NULL,
  `HomeAbout` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `websitesetting`
--

INSERT INTO `websitesetting` (`SettingID`, `WebsiteName`, `WebsiteTitle`, `WebsiteDescription`, `WebsiteContent`, `WebsiteContact`, `FacebookURL`, `InstagramURL`, `TwitterURL`, `GPlusURL`, `YoutubeURL`, `Address`, `Phone`, `Email`, `WebsiteImage`, `LargeLogo`, `SmallLogo`, `HomeAbout`) VALUES
(1, 'Lumba Technology', 'Lumba Technology', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '', '', 'http://facebook.com', 'http://instagram.com', 'http://twitter.com', 'http://plus.google.com', 'http://youtube.com', 'Jl. Mampang Prapatan \nNo.001 block A9 Jakarta Selatan', '021 - 987 987 9876', 'email@domain.com', 'website-image.jpg', 'exe3n9j8jy.png', 'rou6d88b85.png', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`CategoryID`),
  ADD KEY `Position` (`Position`);

--
-- Indexes for table `msadmin`
--
ALTER TABLE `msadmin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `msbrand`
--
ALTER TABLE `msbrand`
  ADD PRIMARY KEY (`BrandID`);

--
-- Indexes for table `mspageinfo`
--
ALTER TABLE `mspageinfo`
  ADD PRIMARY KEY (`PageName`);

--
-- Indexes for table `msproduct`
--
ALTER TABLE `msproduct`
  ADD PRIMARY KEY (`ProductID`);

--
-- Indexes for table `mssliderimg`
--
ALTER TABLE `mssliderimg`
  ADD PRIMARY KEY (`SliderID`),
  ADD KEY `Position` (`Position`);

--
-- Indexes for table `trcategorybrand`
--
ALTER TABLE `trcategorybrand`
  ADD PRIMARY KEY (`CategoryID`,`BrandID`);

--
-- Indexes for table `websitesetting`
--
ALTER TABLE `websitesetting`
  ADD PRIMARY KEY (`SettingID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `CategoryID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=130;
--
-- AUTO_INCREMENT for table `msadmin`
--
ALTER TABLE `msadmin`
  MODIFY `admin_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `msbrand`
--
ALTER TABLE `msbrand`
  MODIFY `BrandID` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `msproduct`
--
ALTER TABLE `msproduct`
  MODIFY `ProductID` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `mssliderimg`
--
ALTER TABLE `mssliderimg`
  MODIFY `SliderID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `websitesetting`
--
ALTER TABLE `websitesetting`
  MODIFY `SettingID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
