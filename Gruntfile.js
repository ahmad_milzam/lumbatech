module.exports = function(grunt) {

  /**
   * Dynamically load npm tasks
   */
  require('load-grunt-tasks')(grunt);

  /**
   * CSSKit Grunt config
   */
  grunt.initConfig({

    /**
     * Read package.json file
     */
    pkg: grunt.file.readJSON('package.json'),

    /**
     * Set project info
     */
    project: {
      bower: 'bower_components',
      src: 'src',
      assets: 'www/assets'
    },

    /**
     * Project banner
     * Dynamically appended to CSS/scripts files
     * Inherits text from package.json
     */
    tag: {
      banner: '/*!\n' +
              ' * <%= pkg.name %>\n' +
              ' * <%= pkg.description %>\n' +
              ' * <%= pkg.repository.url %>\n' +
              ' * @author <%= pkg.author %>\n' +
              ' * @version <%= pkg.version %>\n' +
              ' * Copyright <%= pkg.copyright %>. <%= pkg.license %> licensed.\n' +
              ' * <%= grunt.template.today("yyyy-mm-dd") %>\n' +
              ' */\n'
    },

    /**
     * Compile Sass/SCSS files
     * https://github.com/gruntjs/grunt-sass
     * Compiles all Sass/SCSS files and appends project banner
     */
    sass: {
      options: {
        includePaths: [
          '<%= project.bower %>',
          '<%= project.bower %>/csskit/src/scss'
        ],
        outputStyle: 'nested',
        sourceMap: true
      },
      compile: {
        files: {
          '<%= project.assets %>/styles/main.css' : '<%= project.src %>/scss/main.scss'
        }
      }
    },

    /**
     * Parse CSS and add vendor-prefixed CSS properties
     * using the Can I Use database.
     * Based on Autoprefixer.
     * https://github.com/nDmitry/grunt-autoprefixer
     */
    autoprefixer: {
      compile: {
        options: {
          browsers: [
            'last 2 versions',
            '> 1%',
            'ie 9'
          ],
          map: false
        },
        files: {
          '<%= project.assets %>/styles/main.css' : ['<%= project.assets %>/styles/main.css']
        }
      }
    },

    /**
     * Runs tasks minify the script after prefixed with autoprefixer
     * https://github.com/gruntjs/grunt-contrib-cssmin
     */
    cssmin: {
      options:{
        report: 'gzip'
      },
      target: {
        files: {
          '<%= project.assets %>/styles/main.min.css' : '<%= project.assets %>/styles/main.css'
        }
      }
    },

    /**
     * Concatenate files.
     * https://github.com/gruntjs/grunt-contrib-concat
     */
    concat: {
      options: {
        sourceMap: true
      },
      target: {
        src: [
          // required script
          '<%= project.bower %>/jquery/dist/jquery.js',
          '<%= project.bower %>/slick-carousel/slick/slick.js',
          '<%= project.bower %>/matchHeight/jquery.matchHeight.js',
          //'<%= project.src %>/scripts/ismobile.js',
          '<%= project.src %>/scripts/which-animation.js',
          '<%= project.src %>/scripts/which-transition.js',
          //'<%= project.src %>/scripts/jquery.plugin.menu-aim.js',
          '<%= project.src %>/scripts/jquery.alert.js',
          '<%= project.src %>/scripts/jquery.alertbox.js',
          '<%= project.src %>/scripts/jquery.form.helper.js',
          '<%= project.src %>/scripts/responsive-img.js',
          '<%= project.src %>/scripts/main.js'
        ],
        dest: '<%= project.assets %>/scripts/main.js'
      }
    },

    /**
     * Minify files with UglifyJS.
     * https://github.com/gruntjs/grunt-contrib-uglify
     */
    uglify: {
      options: {
        mangle: true,
        compress: {
          sequences: true,
          dead_code: true,
          conditionals: true,
          booleans: true,
          unused: true,
          if_return: true,
          join_vars: true,
          drop_console: true
        },
        report: 'gzip'
      },
      target: {
        files: {
          '<%= project.assets %>/scripts/main.min.js' : '<%= project.assets %>/scripts/main.js'
        }
      }
    },

    /**
     * Runs tasks against changed watched files
     * https://github.com/gruntjs/grunt-contrib-watch
     * Watching development files and run concat/compile tasks
     */
    watch: {
      grunt: {
        files: ['Gruntfile.js'],
        tasks: ['sass', 'autoprefixer', 'concat']
      },
      sass: {
        files: [
          '<%= project.src %>/scss/**/*.{scss, sass}'
        ],
        tasks: ['sass', 'autoprefixer']
      },
      js: {
        files: [
          '<%= project.src %>/scripts/**/*.js'
        ],
        tasks: ['concat:target']
      }
    }
  });

  /**
   * Base task
   */
  grunt.registerTask('base', [
    'sass', // compile scss to css
    'autoprefixer',
    'concat' // concat multiple js file
  ]);

  /**
   * Default task
   * Run `grunt` on the command line
   */
  grunt.registerTask('default', [
    'base',
    'watch'
  ]);

  /**
   * Build task
   * Run `grunt build` on the command line
   * Then compress all JS/CSS files
   */
  grunt.registerTask('build', [
    'base',
    'cssmin',
    'uglify'
  ]);

}
