<?php
	class pagination_model extends CI_Model{
		function __construct(){parent::__construct();}
		
		function pagination($template){
			
			$style['full_tag_open'] = '<ul class="pagination pagination--responsive">';
			$style['full_tag_close'] = '</ul>';
			// $style['next_link'] = '';
			// $style['prev_link'] = '';
			$style['next_tag_open'] = '<li class="pagination__item">';
			$style['anchor_class'] = 'class="pagination__link is-button"';
			$style['next_link'] = '<li class="pagination__item">&rsaquo;</li>';
			$style['prev_link'] = '<li class="pagination__item">&lsaquo;</li>';
			$style['num_tag_open'] = '<li class="pagination__item">';
			$style['num_tag_close'] = '</li>';
			$style['cur_tag_open'] = '<li class="pagination__item"><a href="" class="pagination__link is-active">';
			$style['cur_tag_close'] = '</a></li>';

			$style['first_tag_open']	= '<li class="pagination__item">';
			$style['first_tag_close']	= '</li>';
			$style['last_tag_open']		= '<li class="pagination__item">';
			$style['last_tag_close']	= '</li>';
			return $style;
		}	
	}
?>
