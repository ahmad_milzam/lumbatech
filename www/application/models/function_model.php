<?php
	class function_model extends CI_Model{

		function clean_input($dirty)
		{
			$f=stripslashes(strip_tags(htmlspecialchars($dirty,ENT_QUOTES)));
			return $f;
		}
		
		function convert_price($price){
			if($this->session->userdata('currency')=="usd"){
				$this->load->model('general_model');
				$setting = $this->general_model->get_website_setting();	
				$price = $price/$setting['USDtoIDR'];
				return $price;
			}
			else{
				return $price;	
			}

		}
		function price_format($price){
			if($this->session->userdata('currency')=="usd"){
				$this->load->model('general_model');
				$setting = $this->general_model->get_website_setting();	
				$price = $price/$setting['USDtoIDR'];
				$price = number_format($price,2,',','.');
				return "USD ".$price;
			}
			else{
				$price = number_format($price,0,',','.');
				return "IDR ".$price;	
			}

		}
		function url_no_space($title){
			$string = preg_replace("/[^a-zA-Z0-9]/","-",$title); 
			$string = preg_replace('/-{2,}/', '-', $string); 
			if(substr($string,strlen($string)-1)=='-'){
				$string = strtolower(substr($string, 0,strlen($string)-1));
			}
			return $string;
		}
		function email_checker($email){
			return preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email);
		}
		
		function rand_alpha_num($length)
		{
			$possible_value = "1234567890abcdefghijklmnopqrstuvwxyz";
			$end_string = "";
			for($x=0;$x<$length;$x++)
			{
				$end_string.=$possible_value[rand(0,25+10)];
			}
			return $end_string;
		}
	}
?>