<?php
	class website_model extends CI_Model{
		function get_website_setting($id = 1){
			$this->db->where('SettingID',$id);
			$this->db->from('websitesetting');
			return $this->db->get()->row_array();
		}
		function submit_setting(){
			$f = $this->function_model;
			$id = 1;
			$name = $f->clean_input($this->input->post('web-name'));
			$title = $f->clean_input($this->input->post('web-title'));
			$desc = $f->clean_input($this->input->post('web-desc'));
			$content = $f->clean_input($this->input->post('web-content'));
			$contact = $f->clean_input($this->input->post('web-contact'));
			$fb = $f->clean_input($this->input->post('web-fb'));
			$ig = $f->clean_input($this->input->post('web-ig'));
			$tw = $f->clean_input($this->input->post('web-tw'));
			$gp = $f->clean_input($this->input->post('web-gp'));
			$youtube = $f->clean_input($this->input->post('web-youtube'));
			$address = $f->clean_input($this->input->post('address'));
			$phone = $f->clean_input($this->input->post('phone'));
			$email = $f->clean_input($this->input->post('email'));
			$home_about = $this->input->post('home-about');
			$return_msg = "";
			$return_status = "error";
			if($title==""){
				$return_msg = "Please insert your website title";
			}
			else if($desc==""){
				$return_msg = "Please write about website description";
			}
			// else if($content==""){
			// 	$return_msg = "Please write about your website in website content";
			// }
			// else if($contact==""){
			// 	$return_msg = "Please insert your contact info";
			// }
			else{
				$data = array(
					"WebsiteName"=>$name,
					"WebsiteTitle"=>$title,
					"WebsiteDescription"=>$desc,
					"WebsiteContent"=>$content,
					"WebsiteContact"=>$contact,
					"FacebookURL"=>$fb,
					"InstagramURL"=>$ig,
					"TwitterURL"=>$tw,
					"GPlusURL"=>$gp,
					"YoutubeURL"=>$youtube,
					"Email"=>$email,
					"Address"=>$address,
					"Phone"=>$phone,
					"HomeAbout"=>$home_about,
				);
				$return_msg = "Website setting has been updated";
				$return_status = "success";
				$this->db->update('websitesetting',$data);
			}
			echo json_encode(array("status"=>$return_status,"message"=>$return_msg));

		}
		function upload_logo(){

			if($this->session->userdata('admin')){

				$return_status = "error";
				$msg = "";
				$img = "";
				if(isset($_FILES['image']['tmp_name'])){
					$img = $_FILES['image']['tmp_name'];
                    $ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
				}
				if($img==""){$msg = "Please browse an image for your product";}
				else if($ext!="png" && $ext!="PNG" && $ext!="jpg" && $ext!="JPG" && $ext!="jpeg"){$msg = "Logo file extension should be .png / .jpg";}
				else{
					$generalSetting = $this->website_model->get_website_setting();
					if(file_exists('./media/img/'.$generalSetting['WebsiteImage'])){
						unlink('./media/img/'.$generalSetting['WebsiteImage']);
					}
					$this->load->model('resize_model');

					$path = "./assets/images/website-image.".$ext;
					move_uploaded_file($img, $path);

					$this->db->where('SettingID',1);
					$this->db->update('websitesetting',array("WebsiteImage"=>"website-image.".$ext));

					$msg = base_url('assets/images/website-image.'.$ext.'?k='.rand(0,100000));
					$return_status = "success";
				}
				echo json_encode(array("message"=>$msg,"status"=>$return_status));
			}
		}
		function upload_header_logo(){

			if($this->session->userdata('admin')){

				$return_status = "error";
				$msg = ""; $img = "";
				$type = $this->function_model->clean_input($this->input->post('type'));
				if(isset($_FILES['image']['tmp_name'])){
					$img = $_FILES['image']['tmp_name'];
                    $ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
				}
				if($img==""){$msg = "Please browse an image for your product";}
				else if($ext!="png" && $ext!="PNG" && $ext!="jpg" && $ext!="JPG" && $ext!="jpeg"){$msg = "Logo file extension should be .png / .jpg";}
				else{
					$generalSetting = $this->website_model->get_website_setting();
					$logoName = $this->function_model->rand_alpha_num(10);
					if(file_exists('./data/logo/'.$generalSetting[$type.'Logo'])){
						unlink('./data/logo/'.$generalSetting[$type.'Logo']);
					}

					$path = "./data/logo/".$logoName.".".$ext;
					move_uploaded_file($img, $path);

					$this->db->where('SettingID',1);
					$this->db->update('websitesetting',array($type."Logo"=>$logoName.".".$ext));

					$msg = base_url('data/logo/'.$logoName.'.'.$ext);
					$return_status = "success";
				}
				echo json_encode(array("message"=>$msg,"status"=>$return_status));
			}
		}
	}
?>