<?php
	class contact_model extends CI_Model{
		function send(){
			$return_msg = "error";
			$return_status = "error";
			$f = $this->function_model;

			

			$data['nama'] = $f->clean_input($this->input->post('nama'));
			$data['email'] = $f->clean_input($this->input->post('email'));
			$data['telepon'] = $f->clean_input($this->input->post('telepon'));
			$data['alamat'] = $f->clean_input($this->input->post('alamat'));
			$data['pesan'] = $f->clean_input($this->input->post('pesan'));

			if($this->input->post('nama_depan')){
				exit();
			}

			if($this->input->post('nama_produk') && $this->input->post('jumlah_produk')){

				$data['nama_produk'] = $f->clean_input($this->input->post('nama_produk'));
				$data['jumlah_produk'] = $f->clean_input($this->input->post('jumlah_produk'));
	
				if($data['nama_produk']==""){
					$return_msg = "Silahkan masukan nama produk";
				}
				if($data['jumlah_produk']==""){
					$return_msg = "Silahkan masukan jumlah produk";
				}


			}
			
			if($data['nama']==""){$return_msg = "Silahkan masukan nama Anda";}
			else if($data['email']==""){$return_msg = "Silahkan masukan email Anda";}
			else if(!$f->email_checker($data['email'])){$return_msg = "Silahkan masukan email Anda dengan benar";}
			else if($data['telepon']==""){$return_msg = "Silahkan masukan no telepon Anda";}
			else if($data['pesan']==""){$return_msg = "Silahkan masukan pesan Anda";}
			else{
				$return_status = "success";
				$return_msg = "Pesan Anda telah terkirim. Kami akan mengirim balasan melalui email Anda segera.";

				$message = $this->load->view('page-email/contact-us', $data, true);
				$this->load->model('email_model');
				$this->email_model->send_email("",'Hubungi Kami',$message,$data['email']);
			}

			return array("status"=>$return_status, "message"=>$return_msg);
		}
	}
?>