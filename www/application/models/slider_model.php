<?php
	class Slider_model extends CI_Model{
		function __construct(){
			parent::__construct();
			$this->load->model('resize_model');
		}

		function get_img_slider(){
			$this->db->from('mssliderimg');
			$this->db->order_by('Position');
			return $this->db->get()->result_array();
		}

		function submit_slider(){
			ini_set('memory_limit', '128M');
			$msg = "";
			$return_status = "error";
			$content = ""; $callFunction = "";
			if($this->session->userdata('admin')){
				$f = $this->function_model;
				$img = $_FILES['slider-file-img']['tmp_name'];
				$img_small = $_FILES['slider-file-img-small']['tmp_name'];
				$link = $f->clean_input($this->input->post('link'));
				if($img!=""){
                    $ext = strtolower(pathinfo($_FILES['slider-file-img']['name'], PATHINFO_EXTENSION));
				}
				if($img_small!=""){
                    $ext_small = strtolower(pathinfo($_FILES['slider-file-img-small']['name'], PATHINFO_EXTENSION));
				}
				if($img=="" || $img_small==""){$msg = "Please browse slider images";}
				else if($ext!="jpg" && $ext!="JPG" && $ext!="jpeg" && $ext!="png" ){$msg = "Product image extension should be .jpg or .png";}
				else{
					$path = "././data/slider/";
					$img_code = $f->rand_alpha_num(6);

					$this->db->insert('mssliderimg',array("Link"=>$link));
					$slider_id = $this->db->insert_id();

					$file_name = $slider_id.'-'.$img_code.'.'.$ext;
					$file_name_small = $slider_id.'-'.$img_code.'.'.$ext_small;
					$this->db->where('SliderID',$slider_id);
					$this->db->update('mssliderimg',array("Filename"=>$file_name,"SmallFilename"=>$file_name_small));

					$this->db->set('Position', '`Position`+ 1', FALSE);
					$this->db->update('mssliderimg');

					//move_uploaded_file($img, $path.$file_name);

					$config["source_image"] = $img;
					$config['new_image'] = $path.$file_name;
					$config["width"] = 1440;
					$config["height"] = 550;
					
					$this->load->library('image_lib', $config);
					$this->image_lib->initialize($config);
					$this->image_lib->fit();
					$this->image_lib->clear();


					//move_uploaded_file($img_small, $path.'small/'.$file_name_small);
					
					$config["source_image"] = $img_small;
					$config['new_image'] = $path.'small/'.$file_name;
					$config["width"] = 480;
					$config["height"] = 360;

					$this->load->library('image_lib', $config);
					$this->image_lib->initialize($config);
					$this->image_lib->fit();
					$this->image_lib->clear();

                    $msg = "success"; $callFunction = "sliderAdded";
                    $return_status = "success-reset";
					$content = '<tr id="slider-img-'.$slider_id.'"><td><img src="'.base_url().'data/slider/'.$file_name.'" class="img-responsive">Link : <a href="'.$link.'">'.$link.'</a></td><td><img src="'.base_url().'data/slider/small/'.$file_name_small.'" class="img-responsive"></td><td><i class="btn fa fa-angle-up btn-sm btn-default js-post" data-url="'.base_url('admin/slider/reposition').'" data-post=\'{"id":"'.$slider_id.'","type":"down"}\'></i> <i class="btn fa fa-angle-down btn-sm btn-default js-post" data-url="'.base_url('admin/slider/reposition').'" data-post=\'{"id":"'.$slider_id.'","type":"up"}\'></i> <i class="btn fa fa-trash-o btn-sm btn-danger js-post" data-url="'.base_url('admin/slider/delete_slider').'" data-post=\'{"id":"'.$slider_id.'"}\' data-confirm="Are you sure want to delete this slider?"></i></td></tr>';
				}
			}
			echo json_encode(array("message"=>$msg, "status"=>$return_status, "callFunction"=>$callFunction, "content"=>$content));
		}
		function delete_slider(){
			$msg = "";
			if($this->session->userdata('admin')){
				$f = $this->function_model;
				$id = $f->clean_input($this->input->post('id'));

				$this->db->where('SliderID',$id);
				$this->db->from('mssliderimg');
				
				$img = $this->db->get()->row_array();
				
				$path = "././data/slider/";

				if(count($img)>0){

					$this->db->where('Position > '.$img['Position']);
					$this->db->set('Position', '`Position`- 1', FALSE);
					$this->db->update('mssliderimg');

					$this->db->where('SliderID',$id);
					$this->db->delete('mssliderimg');

					if(file_exists($path.$img['Filename'])){
						unlink($path.$img['Filename']);
					}
					if(file_exists($path.'small/'.$img['SmallFilename'])){
						unlink($path.'small/'.$img['SmallFilename']);
					}
					echo json_encode(array("status"=>"success","message"=>"success","callFunction"=>"sliderDeleted","id"=>$id));
				}

			}
		}
		function reposition(){
			$msg = "";
			if($this->session->userdata('admin')){
				$f = $this->function_model;
				$id = $f->clean_input($this->input->post('id'));
				$type = $f->clean_input($this->input->post('type'));
				$callFunction = "";
				$this->db->where('SliderID',$id);
				$this->db->from('mssliderimg');
				
				$img = $this->db->get()->row_array();
				$total_slider = count($this->get_img_slider());
				
				$path = "././data/slider/";

				if(count($img)>0){
					if($type=="up")
						$position = $img['Position']+1;
					else
						$position = $img['Position']-1;
					if($position!=0 && $position<$total_slider){
						$this->db->where('Position',$position);
						$this->db->set('Position', $img['Position']);
						$this->db->update('mssliderimg');

						$this->db->where('SliderID',$id);
						$this->db->set('Position',$position);
						$this->db->update('mssliderimg');

						$callFunction = "moveSlider";
					}
					
					echo json_encode(array("status"=>"success","message"=>"success","callFunction"=>$callFunction,"id"=>$id,"type"=>$type));
				}

			}
		}
	}
?>