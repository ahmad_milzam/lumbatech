<?php
	class brand_model extends CI_Model{
		function get_brand($config = array()){
			$config = array_merge(
				array(
					"BrandID"=>"all",
					"BrandKeyname"=>"all",
					"not_BrandID"=>"all",
					"order_by"=>"BrandID",
					"order_value"=>"ASC",
				)
				,$config);
			$this->db->from('msbrand');

			if($config['BrandID']!="all"){
				$this->db->where('BrandID',$config['BrandID']);
			}
			if($config['BrandKeyname']!="all"){
				$this->db->where('BrandKeyname',$config['BrandKeyname']);
			}
			if($config['not_BrandID']!="all"){
				$this->db->where('BrandID != "'.$config['not_BrandID'].'"');
			}

			if($config['BrandID']!="all" || $config['BrandKeyname']!="all"){
				return $this->db->get()->row_array();
			}
			else{
				$this->db->order_by($config['order_by'],$config['order_value']);
				return $this->db->get()->result_array();
			}
		}

		function generate_keyname($name,$brand_id=''){
			$prefix_no = '';
			do{
				if($prefix_no==""){
					$prefix = "";
					$prefix_no = 2;
				}
				else{
					$prefix = "-".$prefix_no;
					$prefix_no++;
				}
				$keyname = $this->function_model->url_no_space($name);
				$keyname_check = $this->get_brand(array("BrandKeyname"=>$keyname.$prefix,"not_BrandID"=>$brand_id));
			}while(count($keyname_check)>0);
			return $keyname.$prefix;

		}
		function submit(){
			$return_msg = "";$return_status = "error";$return_url = "";

			$f = $this->function_model;
			$brand_id = $f->clean_input($this->input->post('brand-id'));
			$name = $f->clean_input($this->input->post('name'));
			$keyname = $f->clean_input($this->input->post('keyname'));
			$description = "";
			$ext=""; $img = "";
			if(isset($_FILES['image']))
				$img = $_FILES['image']['tmp_name'];
			if($img!=""){
                $ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
			}

			if($name==""){
				$return_msg = "Please insert brand name";
			}
			//else if($keyname==""){$return_msg = "Please insert brand name";}
			// else if($ext=="" && $brand_id==""){
			// 	$return_msg = "Please select brand image";
			// }
			else if($ext!="" && $ext!="jpg" && $ext!="JPG" && $ext!="jpeg" && $ext!="png" ){$msg = "Image extension should be .jpg or .png";}
			else{

				if($keyname==""){
					$keyname = $this->generate_keyname($name,$brand_id);
				}
				$keyname_check = $this->get_brand(array("BrandKeyname"=>$keyname,"not_BrandID"=>$brand_id));
				if(count($keyname_check)>0){
					$return_msg = "Keyname already exists";
				}
				else{
					if($brand_id==0){
						$this->db->insert('msbrand',array("BrandName"=>$name,"BrandKeyname"=>$keyname,"Description"=>$description));	
						$brand_id = $this->db->insert_id();
						$return_msg = "Brand has been added";
					}
					else{
						$brand_detail = $this->get_brand(array("BrandID"=>$brand_id));
						$this->db->where('BrandID',$brand_id);
						$this->db->update('msbrand',array("BrandName"=>$name,"BrandKeyname"=>$keyname,"Description"=>$description));	
						$return_msg = "Brand has been updated";
					}

					if($ext!=""){
						$path = "./data/brand/";
						$img_code = $f->rand_alpha_num(6);
						$file_name = $brand_id.'-'.$img_code.'.'.$ext;

						if(isset($brand_detail) && file_exists($path.$brand_detail['BrandFilename']) && $brand_detail['BrandFilename']!=""){
							unlink($path.$brand_detail['BrandFilename']);
						}
						$this->db->where('BrandID',$brand_id);
						$this->db->update('msbrand',array("Filename"=>$file_name));

						move_uploaded_file($img, $path.$file_name);

						$config["source_image"] = $path.$file_name;
						$config['new_image'] = $path.$file_name;
						$config["width"] = 150;
						$config["height"] = 150;
						
						$this->load->library('image_lib', $config);
						$this->image_lib->fit();
					}
					
					$return_url = "self";
					$return_status = "success";
				}
			}

			return json_encode(array("message"=>$return_msg, "status"=>$return_status,"url"=>$return_url));
		}
		function delete(){
			$f = $this->function_model;
			$id = $f->clean_input($this->input->post('id'));

			$return_status = "error";
			$return_msg = "";
			$return_url = "";
			$brand_exist = $this->get_brand(array("BrandID"=>$id));
			if(count($brand_exist)>0){
				$this->db->where('BrandID',$id);
				$this->db->delete('msbrand');	
				if($brand_exist['BrandFilename']!="" && file_exists('./data/brand/'.$brand_exist['BrandFilename'])){
					unlink('./data/brand/'.$brand_exist['BrandFilename']);
				}

				$return_status = "success";
				$return_msg = "Success";
				$return_url = "self";
			}


			return json_encode(array("message"=>$return_msg,"status"=>$return_status,"url"=>$return_url));
		}
	}
?>