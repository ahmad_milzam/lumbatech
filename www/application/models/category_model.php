<?php
	class category_model extends CI_Model{
		function getCategory($config = array()){
			$config = array_merge(
				array(
					"CategoryID"=>"all",
					"CategoryKeyname"=>"all",
				)
				,$config);
			$this->db->from('category');
			if($config['CategoryID']!="all"){
				$this->db->where('CategoryID',$config['CategoryID']);
			}
			if($config['CategoryKeyname']!="all"){
				$this->db->where('CategoryKeyname',$config['CategoryKeyname']);
			}
			if($config['CategoryID']!="all" || $config['CategoryKeyname']!="all"){

				return $this->db->get()->row_array();
			}
			else{
				$this->db->order_by('Position','ASC');
				return $this->db->get()->result_array();
			}
		}
		function generate_keyname($name,$category_id=''){
			$prefix_no = '';
			do{
				if($prefix_no==""){
					$prefix = "";
					$prefix_no = 2;
				}
				else{
					$prefix = "-".$prefix_no;
					$prefix_no++;
				}
				$keyname = $this->function_model->url_no_space($name);
				$keyname_check = $this->get_category_by_keyname($keyname.$prefix, $category_id);
			}while(count($keyname_check)>0);
			return $keyname.$prefix;

		}
		function get_category($parent='all'){
			$this->db->from('category');
			$this->db->order_by('Position','ASC');
			if($parent!='all' || $parent==0){
				$this->db->where('Parent',$parent);
			}

			return $this->db->get()->result_array();
		}

		function get_category_by_keyname($keyname, $category_id = 0){
			$this->db->where('CategoryKeyname',$keyname);
			$this->db->from('category');
			if($category_id!=0)
				$this->db->where('CategoryID != '.$category_id);
			return $this->db->get()->row_array();
		}
		function get_category_by_id($id){
			$this->db->where('CategoryID',$id);
			$this->db->from('category');
			return $this->db->get()->row_array();
		}
		function submit(){
			$return_msg = "";$return_status = "error";$return_url = "";

			$f = $this->function_model;
			$category_id = $f->clean_input($this->input->post('category-id'));
			$name = $f->clean_input($this->input->post('name'));
			$keyname = $f->clean_input($this->input->post('keyname'));
			$description = $f->clean_input($this->input->post('description'));
			$brand = $this->input->post('brand');
			//$parent = $f->clean_input($this->input->post('parent'));
			$parent = 0;
			$ext=""; $img = "";
			if(isset($_FILES['image']))
				$img = $_FILES['image']['tmp_name'];
			if($img!=""){
                $ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
			}

			if($name==""){
				$return_msg = "Please insert category name";
			}
			//else if($keyname==""){ $return_msg = "Please insert category name"; }
			else if($parent==0 && $ext=="" && $category_id==""){
				$return_msg = "Please select category image";
			}
			else if($ext!="" && $ext!="jpg" && $ext!="JPG" && $ext!="jpeg" && $ext!="png" ){$msg = "Image extension should be .jpg or .png";}
			else{
				$this->load->model('category_model');
				if($keyname==""){
					$keyname = $this->generate_keyname($name,$category_id);
				}
				$keyname_check = $this->category_model->get_category_by_keyname($keyname, $category_id);
				if(count($keyname_check)>0){
					$return_msg = "Keyname already exists";
				}
				else{
					if($category_id==0){
						$this->db->insert('category',array("CategoryName"=>$name,"CategoryKeyname"=>$keyname,"Parent"=>$parent,"Description"=>$description));
						$category_id = $this->db->insert_id();
						$return_msg = "Category has been added".$keyname;

						$this->db->set('Position', '`Position`+ 1', FALSE);
						$this->db->update('category');
					}
					else{
						$category_detail = $this->getCategory(array("CategoryID"=>$category_id));
						$this->db->where('CategoryID',$category_id);
						$this->db->update('category',array("CategoryName"=>$name,"CategoryKeyname"=>$keyname,"Parent"=>$parent,"Description"=>$description));
						$return_msg = "Category has been updated";
					}

					$this->db->where('CategoryID',$category_id);
					$this->db->delete('trcategorybrand');

					if($brand!="" && count($brand)>0){
						foreach($brand as $row){
							$this->db->insert('trcategorybrand',array("CategoryID"=>$category_id,"BrandID"=>$row));
						}
					}

					if($ext!=""){
						$path = "./data/category/";
						$img_code = $f->rand_alpha_num(6);
						$file_name = $category_id.'-'.$img_code.'.'.$ext;

						if(isset($category_detail) && file_exists($path.$category_detail['Filename']) && $category_detail['Filename']!=""){
							unlink($path.$category_detail['Filename']);
						}
						$this->db->where('CategoryID',$category_id);
						$this->db->update('category',array("Filename"=>$file_name));

						move_uploaded_file($img, $path.$file_name);

						$config["source_image"] = $path.$file_name;
						$config['new_image'] = $path.$file_name;
						$config["width"] = 150;
						$config["height"] = 150;

						$this->load->library('image_lib', $config);
						$this->image_lib->fit();
					}

					$return_url = "self";
					$return_status = "success";
				}
			}

			return json_encode(array("message"=>$return_msg, "status"=>$return_status,"url"=>$return_url));
		}
		function delete(){
			$this->load->model('category_model');
			$f = $this->function_model;
			$id = $f->clean_input($this->input->post('id'));

			$return_status = "error";
			$return_msg = "";
			$return_url = "";
			$category_exist = $this->category_model->get_category_by_id($id);
			if(count($category_exist)>0){
				$this->db->where('CategoryID',$id);
				$this->db->delete('category');
				if($category_exist['Filename']!="" && file_exists('./data/category/'.$category_exist['Filename'])){
					unlink('./data/category/'.$category_exist['Filename']);
				}


				$this->db->where('Position > '.$category_exist['Position']);
				$this->db->set('Position', '`Position`- 1', FALSE);
				$this->db->update('category');

				$this->db->where('Parent',$id);
				$this->db->delete('category');

				$return_status = "success";
				$return_msg = "Success";
				$return_url = "self";
			}


			return json_encode(array("message"=>$return_msg,"status"=>$return_status,"url"=>$return_url));
		}
		function get_category_brand($config = array()){
			$config = array_merge(
				array(
					"CategoryID"=>"all"
				)
				,$config);
			if($config['CategoryID']!="all")
				$this->db->where('CategoryID',$config['CategoryID']);
			$this->db->from('trcategorybrand a');
			$this->db->join('msbrand b','a.BrandID = b.BrandID');
			return $this->db->get()->result_array();
		}
		function reposition(){
			$msg = "";
			if($this->session->userdata('admin')){
				$f = $this->function_model;
				$id = $f->clean_input($this->input->post('id'));
				$type = $f->clean_input($this->input->post('type'));
				$callFunction = "";
				$this->db->where('CategoryID',$id);
				$this->db->from('category');
				
				$category = $this->db->get()->row_array();

				$total_category = count($this->getCategory());

				if(count($category)>0){
					if($type=="up")
						$position = $category['Position']+1;
					else
						$position = $category['Position']-1;
					if($position!=0 && $position<$total_category){
						$this->db->where('Position',$position);
						$this->db->set('Position', $category['Position']);
						$this->db->update('category');

						$this->db->where('CategoryID',$id);
						$this->db->set('Position',$position);
						$this->db->update('category');

						$callFunction = "moveCategory";
					}
					
					echo json_encode(array("status"=>"success","message"=>"success","callFunction"=>$callFunction,"id"=>$id,"type"=>$type));
				}

			}
		}
	}
?>