<?php
	class email_model extends CI_Model{
		function __construct(){
			parent::__construct();
		}
		function send_email($to="", $subject, $message, $reply_to =""){

			$generalSetting = $this->website_model->get_website_setting();
			if($to=="")
				$to = $generalSetting['WebsiteName']." <".$generalSetting['Email'].">";
				
			$config=array(
				'charset'=>'utf-8',
				'wordwrap'=> TRUE,
				'mailtype' => 'html'
			);

			$this->load->library('email');
			$this->email->initialize($config);

			$result = $this->email
                ->from($generalSetting['WebsiteName'].' <noreply@lumbatech.com>')
                ->reply_to($reply_to)    // Optional, an account where a human being reads.
                ->to($to)
                ->subject($subject)
                ->message($message)
                ->send();
		}

	}
?>