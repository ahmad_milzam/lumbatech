<?php
	class Product_model extends CI_Model{
		function __construct(){
			parent::__construct();
		}
		function get_product($config = array()){
			$config = array_merge(
				array(
					"ProductID"=>"all",
					"CategoryID"=>"all",
					"CategoryKeyname"=>"all",
					"BrandID"=>"all",
					"BrandKeyname"=>"all",
					"page"=>1,
					"limit"=>"all",
				)
				,$config);
			$this->db->select('*, a.Filename as Filename');
			$this->db->from('msproduct a');
			$this->db->join('category b','a.CategoryID = b.CategoryID');
			$this->db->join('msbrand c','a.BrandID = c.BrandID');
			if($config['CategoryKeyname']!="all"){
				$this->db->where('CategoryKeyname',$config['CategoryKeyname']);
			}
			if($config['BrandKeyname']!="all"){
				$this->db->where('BrandKeyname',$config['BrandKeyname']);
			}
			if($config['limit']!="all"){
				$start_data = ($config['page']-1)*$config['limit'];
				$this->db->limit($config['limit'],$start_data);
			}
			if($config['ProductID']!="all"){
				$this->db->where('ProductID',$config['ProductID']);
				return $this->db->get()->row_array();
			}
			else{
				return $this->db->get()->result_array();
			}
		}
		function submit(){
			$f = $this->function_model;
			$id = $f->clean_input($this->input->post('product-id'));
			$name = $f->clean_input($this->input->post('product-name'));
			$category = $f->clean_input($this->input->post('category'));
			$brand = $f->clean_input($this->input->post('brand'));
			$description =$this->input->post('product-desc');
			$seo_description = $f->clean_input($this->input->post('product-seo-desc'));
			$return_url = "";
			$return_msg = ""; $return_status = "error";
			
			$ext=""; $img = "";
			$brosur_ext=""; $brosur_file = "";
			if(isset($_FILES['image']))
				$img = $_FILES['image']['tmp_name'];
			if($img!=""){
                $ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
			}
			if(isset($_FILES['brosur']))
				$brosur_file = $_FILES['brosur']['tmp_name'];
			if($brosur_file!=""){
                $brosur_ext = strtolower(pathinfo($_FILES['brosur']['name'], PATHINFO_EXTENSION));
			}


			if($name==""){$return_msg = "Please insert product name";}
			else if($category==""){$return_msg = "Please insert product category";}
			else if($brand==""){$return_msg = "Please insert product brand";}
			else if($description==""){$return_msg = "Please insert product description";}
			else if($id=="" && $img==""){$return_msg = "Please select product image";}
			else if($ext!="" && $ext!="jpg" && $ext!="JPG" && $ext!="jpeg" && $ext!="png" ){$return_msg = "Image extension should be .jpg or .png";}
			else if($brosur_ext!="" && $brosur_ext!="pdf" && $brosur_ext!="PDF"){$return_msg = "Brosur file extension should be .pdf";}
			else{
				$product_data = array(
					"ProductName"=>$name,
					"ProductDescription"=>$description,
					"CategoryID"=>$category,
					"BrandID"=>$brand,
					"SEODescription"=>$seo_description,
				);

				if($id==""){
					$this->db->insert('msproduct',$product_data);
					$id = $this->db->insert_id();
					$return_msg = "Product has been added";
					$return_status = "success-reset";
				}
				else{
					$this->db->where('ProductID',$id);
					$this->db->update('msproduct',$product_data);
					$product_detail = $this->get_product(array("ProductID"=>$id));
					$return_msg = "Product has been updated";
					$return_status = "success";
				}
				if($brosur_ext!=""){
					if(isset($product_detail)){
						if(file_exists('./data/file/'.$product_detail['BrosurFilename']) && $product_detail['BrosurFilename']!=""){
							unlink('./data/file/'.$product_detail['BrosurFilename']);
						}

					}
					$path = "./data/file/";
					$brosur_code = $f->rand_alpha_num(6);
					$brosur_filename = $id.'-'.$brosur_code.'.'.$brosur_ext;

					$this->db->where('ProductID',$id);
					$this->db->update('msproduct',array("BrosurFilename"=>$brosur_filename));

					move_uploaded_file($brosur_file, './data/file/'.$brosur_filename);
				}
				if($ext!=""){
					$path = "./data/product/";
					$img_code = $f->rand_alpha_num(6);
					$file_name = $f->url_no_space(strtolower($name)).'-'.$id.'.'.$ext;

					if(isset($product_detail)){
						if(file_exists($path.$product_detail['Filename']) && $product_detail['Filename']!=""){
							unlink($path.$product_detail['Filename']);
						}
						if(file_exists($path.'tmb/'.$product_detail['Filename']) && $product_detail['Filename']!=""){
							unlink($path.'tmb/'.$product_detail['Filename']);
						}
						$return_url = "self";
					}

					$this->db->where('ProductID',$id);
					$this->db->update('msproduct',array("Filename"=>$file_name));

					$config["source_image"] = $img;
					$config['new_image'] = $path.$file_name;
					$config["width"] = 650;
					$config["height"] = 490;
					
					$this->load->library('image_lib');
					$this->image_lib->initialize($config);
					$this->image_lib->fit();
					$this->image_lib->clear();

					$config2["source_image"] = $path.$file_name;
					$config2['new_image'] = $path.'tmb/'.$file_name;
					$config2["width"] = 200;
					$config2["height"] = 200;
					
					$this->image_lib->initialize($config2);
					$this->image_lib->fit();
					$this->image_lib->clear();

				}
			}
			echo json_encode(array("message"=>$return_msg, "status"=>$return_status,"url"=>$return_url));

		}
		function show_product(){
			if($this->session->userdata('admin')){
				$f = $this->function_model;
				$product_id = $f->clean_input($this->input->post('product-id'));
				$product = $this->get_product(array("ProductID"=>$product_id));

				if(count($product)>0){
					if($product['Status']==0){
						$status = 1;
						$msg = "On";
					}
					else{
						$status = 0;
						$msg = "Off";
					}
					$this->db->where('ProductID',$product_id);
					$this->db->update('msproduct',array("Status"=>$status));
					return json_encode(array("message"=>$msg,"status"=>"success","callFunction"=>"productActivation","id"=>$product_id));
				}
			}
		}

		function delete(){
			if($this->session->userdata('admin')){
				$msg = "";
				$f = $this->function_model;
				$id = $f->clean_input($this->input->post('product-id'));

				$detail = $this->get_product(array("ProductID"=>$id));
				$return_status = "error";
				$return_msg = "";
				if(count($detail)>0){
					$this->db->where('ProductID',$id);
					$this->db->delete('msproduct');
					$path = './data/product/';
					if(file_exists('./data/file/'.$detail['BrosurFilename']) && $detail['BrosurFilename']!=""){
						unlink('./data/file/'.$detail['BrosurFilename']);
					}
					if(file_exists($path.$detail['Filename']) && $detail['Filename']!=""){
						unlink($path.$detail['Filename']);
					}
					if(file_exists($path.'tmb/'.$detail['Filename']) && $detail['Filename']!=""){
						unlink($path.'tmb/'.$detail['Filename']);
					}
				}
				$return_status = "success";
				$return_msg = "Product has been deleted";
				$callFunction = "productRemoved";
				echo json_encode(array("status"=>$return_status,"message"=>$return_msg,"id"=>$id,"callFunction"=>$callFunction));
			}
		}
		function deleteBrosur(){
			if($this->session->userdata('admin')){
				$msg = "";
				$f = $this->function_model;
				$id = $f->clean_input($this->input->post('product-id'));

				$detail = $this->get_product(array("ProductID"=>$id));
				$return_status = "error";
				$return_msg = "";
				if(count($detail)>0){
					if(file_exists('./data/file/'.$detail['BrosurFilename']) && $detail['BrosurFilename']!=""){
						unlink('./data/file/'.$detail['BrosurFilename']);
					}
					$this->db->where('ProductID',$id);
					$this->db->update('msproduct',array("BrosurFilename"=>""));
				}

				$return_status = "success";
				$return_msg = "Product brosur has been removed";
				$callFunction = "brosurRemoved";

				echo json_encode(array("status"=>$return_status,"message"=>$return_msg,"callFunction"=>$callFunction));
			}
		}
	}
?>