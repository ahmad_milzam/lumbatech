<?php
	class Info_model extends CI_Model{
		function get_info_page($keyname=""){
			$this->db->from('mspageinfo');
			if($keyname!=""){
				$this->db->where('PageName',$keyname);				
				return $this->db->get()->row_array();
			}
			else{
				return $this->db->get()->result_array();
			}
		}
		function submit_info(){
			$f = $this->function_model;
			$name = $f->clean_input($this->input->post('info-id'));
			$content = $this->input->post('content');
			$img = ""; $ext = "";
			$status = "error"; $msg = ""; $img_path = ""; $callFunction = "";
			if(isset($_FILES['image']))
				$img = $_FILES['image']['tmp_name'];
			if($img!=""){
                $ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
			}
			if($ext!="" && $ext!="jpg" && $ext!="JPG" && $ext!="jpeg" && $ext!="png" ){$msg = "Image extension should be .jpg or .png";}
			else{

				if($img!=""){
					$path = "././data/info-img/";
					$img_code = $f->rand_alpha_num(6);

					$file_name = $name.'-'.$img_code.'.'.$ext;
					$prev_data = $this->get_info_page($name);
					if(count($prev_data)>0 && $prev_data['Filename']!="" && file_exists($path.$prev_data['Filename'])){
						unlink($path.$prev_data['Filename']);
					}
					$config["source_image"] = $img;
					$config['new_image'] = $path.$file_name;
					$config["width"] = 1000;
					$config["height"] = 400;
					
					$this->load->library('image_lib', $config);
					$this->image_lib->fit();
					$img_path = base_url('data/info-img/'.$file_name);
					$this->db->where('PageName',$name);
					$this->db->update('mspageinfo',array("Content"=>$content,"Filename"=>$file_name));
				}
				else{

					$this->db->where('PageName',$name);
					$this->db->update('mspageinfo',array("Content"=>$content));	
				}
				$status = "success-reset"; $msg = "Data updated"; $callFunction = "infoSaved";

			}

			echo json_encode(array("message"=>$msg,"status"=>$status, "callFunction"=>$callFunction, "pageName"=>$name, "image"=>$img_path));


		}
	}
?>