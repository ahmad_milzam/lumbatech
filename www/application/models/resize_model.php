<?php
 
class resize_model extends CI_Model {

 
   var $image;
   var $image_type;
   function __construct(){parent::__construct();}
   function load($filename) {
      ini_set('memory_limit', '128M');
      $image_info = getimagesize($filename);
      $this->image_type = $image_info[2];
      if( $this->image_type == IMAGETYPE_JPEG ) {
         $this->image = imagecreatefromjpeg($filename);
      } elseif( $this->image_type == IMAGETYPE_GIF ) {
         $this->image = imagecreatefromgif($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
         $this->image = imagecreatefrompng($filename);
         //imagealphablending($this->image, true);
      }
   }
   
   function save($filename, $image_type=IMAGETYPE_JPEG, $compression=90, $permissions=null) {
      ini_set('memory_limit', '128M');
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image,$filename,$compression);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image,$filename);
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagealphablending( $this->image, false );
         imagesavealpha( $this->image, true );
         imagepng($this->image,$filename);
      }
      if( $permissions != null) {
         chmod($filename,$permissions);
      }
   }
   
   function output($image_type=IMAGETYPE_JPEG) {
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image);
      } elseif( $image_type == IMAGETYPE_GIF ) {
 		 imagegif($this->image);
      } elseif( $image_type == IMAGETYPE_PNG ) {
      	 imagepng($this->image);
      }
   }
   
   function getWidth() {
      return imagesx($this->image);
   }
   
   function getHeight() {
      return imagesy($this->image);
   }
   
   function resizeToHeight($height) {
      ini_set('memory_limit', '128M');
      $ratio = $height / $this->getHeight();
      $width = ceil($this->getWidth() * $ratio);
      $this->resize($width,$height);
   }
 
   function resizeToWidth($width) {
      ini_set('memory_limit', '128M');
      $ratio = $width / $this->getWidth();
      $height = ceil($this->getheight() * $ratio);
      $this->resize($width,$height);
   }
 
   function scale($scale) {
      ini_set('memory_limit', '128M');
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100;
      $this->resize($width,$height);
   }
 
   function resize($width,$height) {
      ini_set('memory_limit', '128M');
      $new_image = imagecreatetruecolor($width, $height);
      imagealphablending($new_image, false);
      imagesavealpha($new_image, true);  

      imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->image = $new_image;
   }      
 
}
?>