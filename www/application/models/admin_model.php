<?php
	class admin_model extends CI_Model{
		function change_password(){
			$f = $this->function_model;
			$old = $f->clean_input($this->input->post('old-pass'));
			$new = $f->clean_input($this->input->post('new-pass'));
			$conf = $f->clean_input($this->input->post('conf-pass'));
			$return_msg = ""; $return_status = "error";
			if($old==""){
				$return_msg = "Please input your old password";
			}
			else if(strlen($new)<6){
				$return_msg = "New password min. 6 characters";
			}
			else if($new!=$conf){
				$return_msg = "New password and confirm password is not match";
			}
			else{
				$this->db->where('admin_id',$this->session->userdata('admin')['admin_id']);
				$this->db->where('password',sha1($old));
				$this->db->from('msadmin');
				$admin = $this->db->get()->row_array();
				if(count($admin)>0){
					$return_status = "success-reset";
					$this->db->where('admin_id',$this->session->userdata('admin')['admin_id']);
					$this->db->update('msadmin',array("Password"=>sha1($new)));
					$return_msg = "Password successfully changed";
				}
				else{
					$return_msg = "You input wrong old password";
				}
			}
			return array("message"=>$return_msg,"status"=>$return_status);
		}
	}
?>