  <!-- Add your site main content here -->
  <!-- page section kontak -->
  <div class="page-section">
    <div class="grid">
      <div class="grid__item small-1">
        <h1 class="page-title">Produk Kami</h1>

        <div class="grid">

          <div class="grid__item small-1 medium-1of3">
            <div class="sidebar">
              <div class="sidebar__head">
                Kategori
              </div>
              <ul class="sidebar__list" id="js-sidebar">
                <?php foreach($category as $row) {?>
                <li class="sidebar__item has-submenu <?=$row['CategoryKeyname']==$current_category_keyname ? 'is-active is-open' : ''?>">
                  <a href="<?=base_url('produk/'.$row['CategoryKeyname'])?>" class="sidebar__link js-openSubmenu">

                    <div class="flag flag--tiny">
                      <div class="flag__img">
                        <img class="img-icon" src="<?=base_url('data/category/'.$row['Filename'])?>" alt="<?=$row['CategoryName']?>">
                      </div>
                      <div class="flag__body">
                        <?=$row['CategoryName']?>
                      </div>
                    </div>
                    <?php if(count($categoryBrand[$row['CategoryID']])>0) {?>
                    <svg class="sidebar__icon icon icon--chevron-thin-right js-toggleSubmenu"><use xlink:href="#icon--chevron-thin-right"></use></svg>
                    <?php } ?>
                  </a>
                  <?php if(isset($categoryBrand[$row['CategoryID']])){ ?>
                  <ul class="sidebar__list sidebar__list--submenu">
                    <?php foreach($categoryBrand[$row['CategoryID']] as $child) {?>
                    <li class="sidebar__item">
                      <a href="<?=base_url('produk/'.$row['CategoryKeyname'].'/'.$child['BrandKeyname'])?>" class="sidebar__link <?php echo ($this->uri->segment(3) === $child['BrandKeyname']) ? 'is-active' : ''; ?>">
                        <?=$child['BrandName']?>
                      </a>
                    </li>
                    <?php } ?>
                  </ul>
                </li>
                <?php } } ?>
              </ul>
            </div>
          </div>

          <div class="grid__item small-1 medium-2of3">
            <!-- product list -->
            <?php if (count($product) > 0): ?>
              <ul class="block-list block-list--small-2 block-list--medium-3">
                <?php foreach($product as $row) {?>
                <li class="block-list__item">
                  <figure class="product-block js-equalColumn">
                    <img class="product-block__img" src="<?=base_url('data/product/tmb/'.$row['Filename'])?>" alt="<?=$row['ProductName']?>">
                    <figcaption class="product-block__caption">
                      <div class="product-block__title">
                        <?=$row['ProductName']?>
                      </div>
                    </figcaption>
                    <a href="<?=base_url('produk/'.$row['ProductID'].'/'.$this->function_model->url_no_space(strtolower($row['ProductName'])))?>" class="product-block__overlay">
                      <svg class="product-block__overlay__icon icon--huge icon icon--forward"><use xlink:href="#icon--forward"></use></svg>
                    </a>
                  </figure>
                </li>
                <?php } ?>
              </ul>
            <?php else: ?>
              <p>
                <strong>Maaf produk yang dimaksud tidak ada.</strong>
              </p>
            <?php endif; ?>
            <!-- /product list -->

            <!-- pagination -->
            <div class="text-right mt">
              <?php echo $this->pagination->create_links(); ?>

            </div>
            <!-- /pagination -->

          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /page section kontak -->