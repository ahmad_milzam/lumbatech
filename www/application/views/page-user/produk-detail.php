
  <!-- Add your site main content here -->
  <!-- page section kontak -->
  <div class="page-section">
    <div class="grid">
      <div class="grid__item small-1">
        <h1 class="page-title"><?=$product['ProductName']?></h1>

        <div class="grid">

          <div class="grid__item small-1 medium-1of3">
            <div class="sidebar">
              <div class="sidebar__head">
                Kategori
              </div>
              <ul class="sidebar__list" id="js-sidebar">
                <?php foreach($category as $row) {?>
                <li class="sidebar__item has-submenu <?=$row['CategoryKeyname']==$product['CategoryKeyname'] ? 'is-active' : ''?>">
                  <a href="<?=base_url('produk/'.$row['CategoryKeyname'])?>" class="sidebar__link js-openSubmenu">

                    <div class="flag flag--tiny">
                      <div class="flag__img">
                        <img class="img-icon" src="<?=base_url('data/category/'.$row['Filename'])?>" alt="<?=$row['CategoryName']?>">
                      </div>
                      <div class="flag__body">
                        <?=$row['CategoryName']?>
                      </div>
                    </div>
                    <?php if(count($categoryBrand[$row['CategoryID']])>0) {?>
                    <svg class="sidebar__icon icon icon--chevron-thin-right js-toggleSubmenu"><use xlink:href="#icon--chevron-thin-right"></use></svg>
                    <?php } ?>
                  </a>
                  <?php if(isset($categoryBrand[$row['CategoryID']])){ ?>
                  <ul class="sidebar__list sidebar__list--submenu">
                    <?php foreach($categoryBrand[$row['CategoryID']] as $child) {?>
                    <li class="sidebar__item">
                      <a href="<?=base_url('produk/'.$row['CategoryKeyname'].'/'.$child['BrandKeyname'])?>" class="sidebar__link <?php echo ($this->uri->segment(3) === $child['BrandKeyname']) ? 'is-active' : ''; ?>">
                        <?=$child['BrandName']?>
                      </a>
                    </li>
                    <?php } ?>
                  </ul>
                </li>
                <?php } } ?>
              </ul>
            </div>
          </div>

          <div class="grid__item small-1 medium-2of3">

            <!-- produk detail content -->

            <div class="mb">
              <img src="<?=base_url('data/product/'.$product['Filename'])?>" alt="<?=$product['ProductName']?>">
            </div>

            <div class="grid">
              <div class="grid__item small-1of2">
                <div class="h5">Deskripsi Produk</div>
              </div>
              <div class="grid__item small-1of2 text-right">
                <?php if ($product && !empty($product['BrosurFilename'])): ?>
                  <a href="<?=base_url('produk/download/'.$product['BrosurFilename'])?>" class="btn btn--small btn--secondary">Download Brosur</a>
                <?php endif ?>
              </div>
            </div>

            <?=$product['ProductDescription']?>
            <!-- produk detail content -->

            <a href="<?=base_url('hubungi/'.$product['ProductID'])?>" class="btn btn--full btn--with-icon">
              <span class="btn__text">Pesan Sekarang</span>
              <svg class="btn__icon icon icon--chevron-thin-right"><use xlink:href="#icon--chevron-thin-right"></use></svg>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /page section kontak -->
