  <div class="page-footer text-center">
    <h2 class="page-title mb-">Cari Tahu Kami Lebih</h2>

    <!-- list social media -->
    <ul class="list-inline mb">
      <?php if($web_setting['FacebookURL']!="") {?>
      <li class="list-inline__item ph-">
        <a href="<?=$web_setting['FacebookURL']?>">
          <svg class="icon icon--footer icon--facebook"><use xlink:href="#icon--facebook"></use></svg>
        </a>
      </li>
      <?php } ?>
      <?php if($web_setting['TwitterURL']!="") {?>
      <li class="list-inline__item ph-">
        <a href="<?=$web_setting['TwitterURL']?>">
          <svg class="icon icon--footer icon--twitter"><use xlink:href="#icon--twitter"></use></svg>
        </a>
      </li>
      <?php } ?>
      <?php if($web_setting['InstagramURL']!="") {?>
      <li class="list-inline__item ph-">
        <a href="<?=$web_setting['InstagramURL']?>">
          <svg class="icon icon--footer icon--instagram"><use xlink:href="#icon--instagram"></use></svg>
        </a>
      </li>
      <?php } ?>
      <?php if($web_setting['GPlusURL']!="") {?>
      <li class="list-inline__item ph-">
        <a href="<?=$web_setting['GPlusURL']?>">
          <svg class="icon icon--footer icon--googleplus"><use xlink:href="#icon--googleplus"></use></svg>
        </a>
      </li>
      <?php } ?>
      <?php if($web_setting['YoutubeURL']!="") {?>
      <li class="list-inline__item ph-">
        <a href="<?=$web_setting['YoutubeURL']?>">
          <svg class="icon icon--footer icon--youtube"><use xlink:href="#icon--youtube"></use></svg>
        </a>
      </li>
      <?php } ?>
    </ul>
    <!-- /list social media -->
    <!-- list link kategori -->
    <ul class="list-inline mb">
      <?php foreach($categoryFooter as $row) {?>
      <li class="list-inline__item ph-">
        <a class="text-underline" href="<?=base_url('produk/'.$this->function_model->url_no_space($row['CategoryKeyname']))?>"><?=$row['CategoryName']?></a>
      </li>
      <?php } ?>
    </ul>
    <!-- /list link kategori -->

    <!-- address -->
    <div class="mb">
      <strong><?=$web_setting['WebsiteName']?></strong>
      <br><?=nl2br($web_setting['Address'])?>
      <br>
      Telp. <?=$web_setting['Phone']?>
    </div>
    <!-- /address -->

    <!-- copyright -->
    <small>
      Copyright &copy; lumbatech.com <?php echo Date('Y'); ?>
      | Web design and development by <a href="http://jakartaweb.com">Jakartaweb</a>
    </small>
  <!-- /copyright -->
  </div>

  <!-- Put JavaScript at bottom of your page before closing body tag -->
  <script src="<?=base_url()?>assets/scripts/main<?php echo ENVIRONMENT == 'development' ? '.js' : '.min.js' ?>"></script>

  <!-- Google Analytics: change UA-XXXXX-X to be your site's ID -->
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-70998547-1', 'auto');
    ga('send', 'pageview');
  </script>
  <!-- Built with love using Web Starter Kit -->
</body>
</html>