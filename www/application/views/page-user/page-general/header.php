<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?=isset($web_title) ? $web_title : $web_setting['WebsiteTitle']?></title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">

  <meta name="description" content="DESCRIPTION_DEFAULT | DESCRIPTION_PRODUCT">

  <meta property="og:title" content="<?=isset($web_title) ? $web_title.' | Lumbatech' : $web_setting['WebsiteTitle']?>">
  <meta property="og:url" content="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
  <meta property="og:image" content="<?=isset($og_img) ? $og_img : base_url('media/img/website-image.png')?>">
  <meta property="og:description" content="<?=isset($meta_description) ? $meta_description : $web_setting['WebsiteDescription']?>">
  <meta property="og:type" content="website">
  <meta property="og:site_name" content="<?=$web_setting['WebsiteName']?>">

  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="<?=isset($web_title) ? $web_title.' | Lumbatech' : 'Lumbatech'?>">
  <meta name="twitter:description" content="<?=isset($meta_description) ? $meta_description : $web_setting['WebsiteDescription']?>">
  <meta name="twitter:image:src" content="<?=isset($og_img) ? $og_img : base_url('media/img/'.$web_setting['WebsiteImage'])?>">

  <!-- Add to homescreen for Chrome on Android -->
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="application-name" content="Lumbatech">

  <!-- Add to homescreen for Safari on iOS -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="white">
  <meta name="apple-mobile-web-app-title" content="Lumbatech">

  <!-- Page styles -->
  <link rel="stylesheet" href="<?=base_url()?>assets/styles/main<?php echo ENVIRONMENT == 'development' ? '.css' : '.min.css' ?>">
  <link href='https://fonts.googleapis.com/css?family=Roboto:700,500,400,400italic' rel='stylesheet' type='text/css'>
</head>
<body>

  <!-- Warn user for their outdated browser -->
  <!--[if lt IE 10]>
    <div class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</div>
  <![endif]-->
    <div class="page-head">
    <div class="page-wrapper">
      <nav class="site-nav">
        <!-- logo -->
        <a href="<?=base_url()?>" class="site-nav__logo">
          <img class="site-nav__logo-img hide-for-large-up" src="<?=base_url()?>data/logo/<?=$web_setting['SmallLogo']?>" alt="logo"/>
          <img class="site-nav__logo-img show-for-large-up" src="<?=base_url()?>data/logo/<?=$web_setting['LargeLogo']?>" alt="logo"/>
        </a>
        <!-- logo -->

        <!-- phone -->
        <span class="site-nav__phone">
          <svg class="icon icon--phone"><use xlink:href="#icon--phone"></use></svg>
          <?=$web_setting['Phone']?>
        </span>
        <!-- phone -->

        <!-- navigation link -->
        <ul class="site-nav__list">
          <li class="site-nav__item">
            <a class="site-nav__link <?=$content=="home" ? 'is-active' : ''?>" href="<?=base_url()?>">Beranda</a>
          </li>
          <li class="site-nav__item">
            <a class="site-nav__link <?=strpos($content,'produk')!==false ? 'is-active' : ''?>" href="<?=base_url('produk')?>">Produk Kami</a>
          </li>
          <li class="site-nav__item">
            <a class="site-nav__link <?=$content=="tentang" ? 'is-active' : ''?>" href="<?=base_url('tentang')?>">Tentang Kami</a>
          </li>
          <li class="site-nav__item">
            <a class="site-nav__link <?=$content=="hubungi" ? 'is-active' : ''?>" href="<?=base_url('hubungi')?>">Hubungi Kami</a>
          </li>
        </ul>
        <!-- navigation link -->
      </nav>
    </div>
  </div>

  <!-- breadcrumbs -->
  <?php if(isset($breadcrumbs)) {?>
  <div class="page-section page-section--small">
    <div class="grid">
      <div class="grid__item small-1">
        <ul class="breadcrumb">
          <?php foreach($breadcrumbs as $value => $link) {?>
          <li class="breadcrumb__item"><a href="<?=$link?>" class="breadcrumb__item__link"><?=$value?></a></li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </div>
  <?php } ?>
<!-- /breadcrumbs -->