
  <!-- Add your site main content here -->
  <!-- page section kontak -->
  <div class="page-section">
    <div class="grid">
      <div class="grid__item small-1">
        <h1 class="page-title">Hubungi Kami</h1>
        <p class="text-center">
          Kami siap menemukan solusi pintar bagi anda
          <br>
          <strong>Hubungi <?=$web_setting['Phone']?> atau isi formulir di bawah ini.</strong>
        </p>
        <div class="grid">
          <div class="grid__item small-1 medium-2of3 medium-centered">

            <form action="<?=base_url('hubungi/kirim')?>" method="post" id="js-formContact">
              <!-- alert -->
              <div id="js-alertContainer"></div>
              <!-- /alert -->

              <?php if(count($product)>0) {?>
              <!-- tampilkan field ini kalau ada id produk yg di pesan -->
              <div class="grid">
                <div class="grid__item small-1 medium-1of2">
                  <div class="field-row">
                    <label class="label label--required small-1" for="nama">Nama Produk</label>
                    <input class="input small-1" id="nama-product" name="nama_produk" type="text" value="<?=$product['ProductName']?>" placeholder="Nama Produk" required>
                  </div>
                </div>
                <div class="grid__item small-1 medium-1of2">
                  <div class="field-row">
                    <label class="label label--required small-1" for="nama">Jumlah Produk</label>
                    <input class="input small-1" id="jumlah" name="jumlah_produk" type="tel" value ="1" placeholder="Jumlah Pesanan Anda" required pattern="[0-9]+">
                  </div>
                </div>
              </div>
              <?php } ?>

              <!-- tampilkan field ini kalau ada id produk yg di pesan -->

              <div class="field-row">
                <label class="label label--required small-1" for="nama">Nama</label>
                <input class="input small-1" id="nama" name="nama" type="text" placeholder="Masukan nama Anda" required>
                <input class="input-important" id="nama_depan" name="nama_depan" type="text">
              </div>
              <div class="field-row">
                <label class="label label--required small-1" for="email">Email</label>
                <input class="input small-1" id="email" name="email" type="email" placeholder="Masukan email Anda" required>
              </div>
              <div class="field-row">
                <label class="label label--required small-1" for="tel">No. Telepon</label>
                <input class="input small-1" id="tel" name="telepon" type="tel" placeholder="Masukan no telepon Anda" required pattern="[0-9]+">
              </div>
              <div class="field-row">
                <label class="label small-1" for="alamat">Alamat</label>
                <textarea class="input input--textarea small-1" name="alamat" id="alamat" placeholder="Masukan alamat Anda"></textarea>
              </div>
              <div class="field-row">
                <label class="label label--required small-1" for="pesan">Pesan</label>
                <textarea class="input input--textarea small-1" name="pesan" id="pesan" placeholder="Masukan Pesan Anda" required></textarea>
              </div>
              <div class="field-row text-right">
                <button class="btn btn--with-icon" type="submit">
                  <span class="btn__text">
                    Kirim Pesan
                  </span>
                  <svg class="btn__icon icon icon--mail"><use xlink:href="#icon--mail"></use></svg>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /page section kontak -->
