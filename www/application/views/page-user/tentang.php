
  <!-- Add your site main content here -->
  <!-- page section kontak -->
  <div class="page-section">
    <div class="grid">
      <article class="grid__item small-1 medium-3of4 medium-centered">
        <h1 class="page-title">Tentang Kami</h1>

        <p>
          <img src="<?=base_url('data/info-img/'.$text['Filename'])?>" class="img-responsive" alt="">
        </p>

        <?=$text['Content']?>
      </article>
    </div>
  </div>
  <!-- /page section kontak -->