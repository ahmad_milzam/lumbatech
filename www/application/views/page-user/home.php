  <!-- Add your site main content here -->

  <!-- big slide -->
  <?php if (count($slider) > 0): ?>
    <div class="slide-wrapper">
      <button class="slide-nav slide-nav--prev">
        <svg class="icon--large icon icon--chevron-thin-left"><use xlink:href="#icon--chevron-thin-left"></use></svg>
      </button>
      <button class="slide-nav slide-nav--next">
        <svg class="icon--large icon icon--chevron-thin-right"><use xlink:href="#icon--chevron-thin-right"></use></svg>
      </button>
      <div class="slide" id="js-slider">
        <?php foreach($slider as $row) {?>
        <a href="<?=$row['Link']?>" class="slide__item">
          <img class="img-responsive" src="<?=base_url()?>assets/images/blank.png" alt="banner image"
            data-src-base="<?=base_url()?>data/slider/"
            data-src="<760:small/<?=$row['SmallFilename']?>,
                      >760:<?=$row['Filename']?>"/>
        </a>
        <?php } ?>
      </div>
    </div>
    <!-- /big slide -->
  <?php endif ?>

  <!-- page section produk -->
  <div class="page-section">
    <div class="grid">
      <div class="grid__item small-1">
        <h2 class="page-title">Produk Kami</h2>
        <div class="grid text-center">
          <?php foreach($category as $row) {?>
          <div class="grid__item small-1 medium-1of2 large-1of4">
            <figure class="product-block js-equalColumn">
              <img class="product-block__img product-block__img--category" src="<?=base_url('data/category/'.$row['Filename'])?>" alt="<?=$row['CategoryName']?>">
              <figcaption class="product-block__caption">
                <div class="product-block__title">
                  <?=$row['CategoryName']?>
                </div>
                <div class="product-block__desc">
                  <?=nl2br($row['Description'])?>
                </div>
              </figcaption>
              <a href="<?=base_url('produk/'.$this->function_model->url_no_space($row['CategoryKeyname']))?>" class="product-block__overlay">
                <svg class="product-block__overlay__icon icon--huge icon icon--forward"><use xlink:href="#icon--forward"></use></svg>
              </a>
            </figure>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div><!-- /page section produk -->

  <!-- page section tentang perusahaan -->
  <div class="page-section">
    <div class="grid">
      <article class="grid__item small-1">
        <h2 class="page-title">Tentang Kami</h2>
        <?=$web_setting['HomeAbout']?>
      </article>
    </div>
  </div>
  <!-- /page section tentang perusahaan -->

  <!-- page section kenapa -->
  <div class="page-section page-section--gray">
    <div class="grid">
      <div class="grid__item small-1">
        <h2 class="page-title">Mengapa Kami</h2>
        <div class="grid text-center text-primary">
          <div class="grid__item small-1 medium-1of2 large-1of4">
            <svg class="icon icon--large icon--tick"><use xlink:href="#icon--tick"></use></svg>
            <h5>Produk Berkualitas</h5>
          </div>
          <div class="grid__item small-1 medium-1of2 large-1of4">
            <svg class="icon icon--large icon--thumb-up"><use xlink:href="#icon--thumb-up"></use></svg>
            <h5>After Sales Terbaik</h5>
          </div>
          <div class="grid__item small-1 medium-1of2 large-1of4">
            <svg class="icon icon--large icon--gear"><use xlink:href="#icon--gear"></use></svg>
            <h5>Tim Instalasi Handal</h5>
          </div>
          <div class="grid__item small-1 medium-1of2 large-1of4">
            <svg class="icon icon--large icon--tie"><use xlink:href="#icon--tie"></use></svg>
            <h5>Layanan Profesional</h5>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /page section kenapa -->

  <!-- page section penawaran -->
  <div class="page-section">
    <div class="grid text-center">
      <div class="grid__item small-1">
        <p class="mb-">
          Kami Siap Memberikan Solusi "Smart Products" Sesuai Dengan Kebutuhan Anda<br>
          <strong>Dan Dapatkan Penawaran Khusus Dari Kami !</strong>
        </p>
        <a href="<?=base_url('hubungi')?>" class="btn btn--with-icon">
          <span class="btn__text">
            Konsultasi Sekarang
          </span>
          <svg class="btn__icon icon icon--chevron-thin-right"><use xlink:href="#icon--chevron-thin-right"></use></svg>
        </a>
      </div>
    </div>
  </div>
  <!-- /page section penawaran -->