<div class="row">
    <?php foreach($info as $key => $row) {?>
	<div class="col-md-9">
	    <form action="<?=base_url()?>admin/info/submit" id="info-form-<?=$row['PageName']?>" method="post" enctype="multipart/form-data">
	    	<input type="hidden" readonly name="info-id" value="<?=$row['PageName']?>">
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'><?=$row['PageTitle']?></small></h3>
                    <div class="pull-right box-tools">
                        <button class="btn btn-default btn-sm" data-widget='collapse' onclick="return false;" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class='box-body pad'>
                    <?php if($row['Filename']!="") {?>
                    <img src="<?=base_url('data/info-img/'.$row['Filename'])?>" class="info-img img-responsive" alt="">
                    <?php } ?>
                    <input type="file" name="image" class="form-control form-reset mb-10">
                    <textarea class='tinymce' name="content"><?=$row['Content']?></textarea>
                </div>
                <div class="box-footer">
                    <div class="alert alert-danger animated form-msg" style="display:none;"><i class="fa fa-ban"></i> &nbsp;<span></span></div>
                	<input type="submit" class="btn btn-success" value="SAVE INFO" onclick="jquery_form('#info-form-<?=$row['PageName']?>');">  
                </div>
            </div>
    	</form>
    </div>
    <?php } ?>
</div>
<script>
    function infoSaved(response){

        if(response.status=="success-reset" && response.image!=""){
            $("#info-form-"+response.pageName+' .info-img').attr('src',response.image);
        }
    }
</script>