<div class="row">

	<div class="col-md-4">
		<div class="box box-info table-responsive">
			<div class="box-header">
				<div class="box-title">Admin Password Manager</div>
			</div>
			<form action="<?=base_url()?>admin/dashboard/changePassword" method="post" id="change-password-form">
				<div class="box-body">
					<label>Old Password</label><br/>
					<input type="password" class="form-control form-reset change-password-txt" name="old-pass"><br/>
					<label>New Password</label><br/>
					<input type="password" class="form-control form-reset change-password-txt" name="new-pass"><br/>
					<label>Confirm New Password</label><br/>
					<input type="password" class="form-control form-reset change-password-txt" name="conf-pass"><br/>
					<div class="alert alert-danger animated form-msg"><i class="fa fa-ban"></i> <span></span></div>
					<input type="submit" class="btn-block btn btn-success" value="CHANGE PASSWORD" onclick="submit_form('#change-password-form'); return false;">
				</div>
			</form>
		</div>

	</div>
</div>
<script type="text/javascript" src="<?=base_url()?>media/admin/js/dashboard.js"></script>