<div class="col-md-3">
    <form action="<?=base_url('admin/brand/submit')?>" enctype="multipart/form-data" method="post" id="add-brand-form">
        <div class="box box-warning">
            <div class="box-header"><div class="box-title"><?=$edit['BrandID']!=0 ? 'EDIT' : 'ADD' ?> brand</div></div>
            <div class="box-body">
                <input type="hidden" value="<?=$edit['BrandID']!=0 ? $edit['BrandID'] : 0 ?>" name="brand-id">
                <label>Brand Name</label>
                <input type="text" class="form-control mb-10" name="name" value="<?=$edit['BrandID']!=0 ? $edit['BrandName'] : '' ?>" placeholder="Your Brand Title">
                <label>Brand URL Title</label>
                <input type="text" class="form-control mb-10" name="keyname" value="<?=$edit['BrandID']!=0 ? $edit['BrandKeyname'] : '' ?>" placeholder="your-brand-title">

                <input type="submit" class="btn mb-10 btn-success btn-block" value="<?=$edit!=0 ? 'EDIT' : 'ADD' ?> brand" onclick="jquery_form('#add-brand-form');">
                <?php if($edit!=0){ ?>
                <a href="<?=base_url('admin/brand')?>"><input type="button" class="btn btn-warning btn-block" value="CANCEL"></a>
                <?php } ?>
            </div>
        </div>
    </form>
</div>
<div class="col-md-9">
    <div class="box box-info">
        <table class="table">
            <thead>
                <tr>
                    <th>Brand Name</th>
                    <th>Brand Keyname</th>
                    <th width="100"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($brand as $row) {?>
                <tr>
                    <td><?=$row['BrandName']?></td>
                    <td><?=$row['BrandKeyname']?></td>
                    <td>
                        <a href="<?=base_url('admin/brand?e='.$row['BrandID'])?>"><i class="fa fa-edit btn btn-sm btn-info" ></i></button></a>
                        <i class="btn fa fa-trash-o btn-sm btn-danger js-post" data-url="<?=base_url('admin/brand/delete')?>" data-post='{"id":"<?=$row['BrandID']?>"}' data-confirm="Are you sure want to delete this brand?"></i>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>


</div>