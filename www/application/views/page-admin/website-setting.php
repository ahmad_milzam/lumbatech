<div class="row">
	<div class="col-md-6">
		<label>Header Logo Large</label>
		<br/>
		<img src="<?=base_url('data/logo/'.$setting['LargeLogo'])?>" id="large-logo">
		<br/>
		<br/>
		<form action="<?=base_url('admin/setting/upload_header_logo')?>" method="post" id="upload-logo-large" enctype="multipart/form-data"><input type="hidden" name="type" value="Large"><input type="file" name="image" class="form-reset" onchange="upload_header_logo('large'); return false;" id="logo-file-large"><br/></form>
		<small>Max. size: 290 x 60 pixel</small>
	</div>
	<div class="col-md-6">
		<label>Header Logo Small</label>
		<br/>
		<img src="<?=base_url('data/logo/'.$setting['SmallLogo'])?>" id="small-logo">
		<br/>
		<br/>
		<form action="<?=base_url('admin/setting/upload_header_logo')?>" method="post" id="upload-logo-small" enctype="multipart/form-data"><input type="hidden" name="type" value="Small"><input type="file" name="image" class="form-reset" onchange="upload_header_logo('small'); return false;" id="logo-file-small"><br/></form>
		<small>Max. size: 60 x 60 pixel</small>
	</div>
</div>
<hr/>
	<form action="<?=base_url('admin/setting/upload_logo')?>" method="post" id="upload-logo-form" enctype="multipart/form-data"><input type="file" name="image" class="form-reset" onchange="upload_logo(); return false;" id="logo-file"><br/></form>

	<form action="<?=base_url('admin/setting/submit_setting')?>" method="post" id="web-setting-form">
	<div class="row">
		<div class="col-md-6">
			<label>Default Shareable Image (.png or .jpg ; min. size 600 x 315. max. size 1200 x 630)</label>
			<br/>
			<div id="logo-content" class="mb-10">
				<img src="<?=base_url()?>assets/images/<?=$setting['WebsiteImage']?>" class="img-responsive">
			</div>



			<label>Website Name</label>
			<input type="text" class="form-control" name="web-name" value="<?=$setting['WebsiteName']?>"><br/>
			<label>Default Website Title (SEO)</label>
			<input type="text" class="form-control" name="web-title" value="<?=$setting['WebsiteTitle']?>"><br/>
			<label>Default Meta Description (SEO) - 150 chars</label>
			<textarea maxlength="150" class="form-control" name="web-desc"><?=$setting['WebsiteDescription']?></textarea><br/>
			<label>Home About</label>
			<textarea class="tinymce" name="home-about" rows="7"><?=$setting['HomeAbout']?></textarea><br/>
		</div>
		<div class="col-md-6">
			<label>Phone Number</label>
			<input type="text" class="form-control" name="phone" value="<?=$setting['Phone']?>"><br/>
			<label>Email</label>
			<input type="text" class="form-control" name="email" value="<?=$setting['Email']?>"><br/>
			<label>Address</label>
			<textarea class="form-control" name="address"><?=$setting['Address']?></textarea><br/>
			<label>Facebook URL</label>
			<input type="text" class="form-control" name="web-fb" value="<?=$setting['FacebookURL']?>"><br/>
			<label>Instagram URL</label>
			<input type="text" class="form-control" name="web-ig" value="<?=$setting['InstagramURL']?>"><br/>
			<label>Twitter URL</label>
			<input type="text" class="form-control" name="web-tw" value="<?=$setting['TwitterURL']?>"><br/>
			<label>Google+ URL</label>
			<input type="text" class="form-control" name="web-gp" value="<?=$setting['GPlusURL']?>"><br/>
			<label>Youtube URL</label>
			<input type="text" class="form-control" name="web-youtube" value="<?=$setting['YoutubeURL']?>"><br/>
		</div>
		<div class="col-md-12">
			<div class="alert alert-success alert-dismissable animated form-msg" >
	            <i class="fa fa-check"></i>&nbsp;&nbsp;
	            <span></span>
	        </div>
			<button type="submit" class="btn btn-primary btn-block" onclick="tinymce.triggerSave(); submit_form('#web-setting-form'); return false;">UPDATE INFO</button>
		</div>
	</form>
</div>
<script>
function upload_logo(){
	$("#upload-logo-form").ajaxForm({
		success: function(result){
          	var result = $.parseJSON(result);
			if(result.status=="success")
				$("#logo-container").css('background-image','url('+result.message+')');
			$("#logo-file").val('');
		}
	}).submit();
	return false;
}

function upload_header_logo(type){
	$("#upload-logo-"+type).ajaxForm({
		success: function(result){
          	var result = $.parseJSON(result);
			if(result.status=="success")
				$("#"+type+"-logo").attr('src',result.message);
			$("#logo-file-"+type).val('');
		}
	}).submit();
	return false;
}
</script>