<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Administrator</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="<?=base_url()?>assets/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/admin/css/ryan-editor.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/admin/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/admin/css/animate.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/admin/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />

    <?php if(isset($css)) { foreach($css as $row){ ?>
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/<?=$row?>.css">
    <?php } }?>
  </head>
  <body class="skin-blue">
    <a href="<?=base_url()?>" id="base_url"></a>
    <header class="header">
      <a href="<?=base_url()?>admin" class="logo">ADMIN</a>
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-right">
          <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="glyphicon glyphicon-user"></i>
                <span>Admin<i class="caret"></i></span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header bg-light-blue">
                    <img src="<?=base_url()?>assets/admin/img/avatar2.png" class="img-circle" alt="User Image" />
                    <p>
                        Administrator
                    </p>
                </li>
                <!-- Menu Body -->
                <!-- Menu Footer-->
                <li class="user-footer">
                    <div class="pull-right">
                        <a href="<?=base_url()?>admin/doLogout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <div class="wrapper row-offcanvas row-offcanvas-left">
      <?=$this->load->view('page-admin/page-general/menu')?>
      <aside class="right-side">
        <section class="content-header">
          <h1><?=$sub_menu?></h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active"><?=$sub_menu?></li>
          </ol>
        </section>
        <section class="content">
          <?=$this->load->view('page-admin/'.$content)?>
        </section>
      </aside>
    </div>