
    <div id="loading-overlay"> <div id="loading"><i class="fa fa-spin fa-spinner"></i></div></div>
    <script src="<?=base_url()?>assets/admin/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>assets/admin/js/jquery-form.js" type="text/javascript"></script>
    <script src="<?=base_url()?>assets/admin/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- date-range-picker -->
    <script src="<?=base_url()?>assets/admin/js/AdminLTE/app.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/admin/js/general-script.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/admin/js/category.js"></script>
    
    <?php if(isset($js)) { foreach($js as $row){ ?>
    <script type="text/javascript" src="<?=base_url()?>assets/admin/js/<?=$row?>.js"></script>
    <?php } }?>
    <script>
        function upload_logo(){
            $("#upload-logo-form").ajaxForm({ 
                success: function(result){
                    var result = $.parseJSON(result);
                    if(result.status=="success")
                        $("#logo-content img").attr('src',result.message);
                    $("#logo-file").val('');
                } 
            }).submit(); 
            return false;
        }
    </script>
  </body>
</html>