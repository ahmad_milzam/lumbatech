<!-- Left side column. contains the logo and sidebar -->
      <aside class="left-side sidebar-offcanvas">                
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url()?>assets/admin/img/avatar2.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>Hello, Admin</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <ul class="sidebar-menu">
            <li <?=($content=='home' ? 'class="active"' : '')?>>
              <a href="<?=base_url()?>admin">
                  <i class="fa fa-home"></i> <span>Dashboard</span>
              </a>
            </li>
            <li <?=($content=='slider' ? 'class="active"' : '')?>>
              <a href="<?=base_url()?>admin/slider">
                  <i class="fa fa-picture-o"></i> <span>Slider</span>
              </a>
            </li>
            <li class="<?=($content=='product-list' || $content=='product-detail' || $content=='add-product' || $content=='edit-product' ? 'active' : '')?>">
              <a href="<?=base_url('admin/product/lists')?>">
                  <i class="fa fa-truck"></i>
                  <span>Product</span>
              </a>
            </li>
            <li >
              <a href="<?=base_url()?>admin/info">
                  <i class="fa fa-file"></i> <span>Info Pages</span>
              </a>
            </li>
            <li <?=($content=='category-setting' ? 'class="active"' : '')?>>
              <a href="<?=base_url()?>admin/category">
                  <i class="fa fa-gear"></i> <span>Category Settings</span>
              </a>
            </li>
            <li <?=($content=='brand-setting' ? 'class="active"' : '')?>>
              <a href="<?=base_url()?>admin/brand">
                  <i class="fa fa-gear"></i> <span>Brand Settings</span>
              </a>
            </li>
            <li <?=($content=='website-setting' ? 'class="active"' : '')?>>
              <a href="<?=base_url()?>admin/setting">
                  <i class="fa fa-globe"></i> <span>Website Setting</span>
              </a>
            </li>
          </ul>
        </section>
      </aside>