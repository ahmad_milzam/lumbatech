<form action="<?=base_url()?>admin/product/submit" method="post" id="add-product-form" enctype="multipart/form-data">
	<div class="row">
		<div class="col-md-8">
			<input type="hidden" value="<?=isset($product) ? $product['ProductID'] : ''?>" name="product-id">
		    <div class="form-group">
		        <label>Product Name</label>
		        <input type="text" class="form-control" placeholder="Product Name" name="product-name" value="<?=isset($product) ? $product['ProductName'] : ''?>">
		    </div>
		    <div class="form-group">
		        <label>Category</label>
		        <select name="category" class="form-control">
		        	<option value="">Select Category</option>
		        	<?php foreach($category as $row) {?>
		        	<option value="<?=$row['CategoryID']?>" <?=isset($product) && $product['CategoryID']==$row['CategoryID'] ? 'selected' : ''?>><?=$row['CategoryName']?></option>
		        	<?php } ?>
		        </select>
		    </div>
		    <div class="form-group">
		        <label>Brand</label>
		        <select name="brand" class="form-control">
		        	<option value="">Select Brand</option>
		        	<?php foreach($brand as $row) {?>
		        	<option value="<?=$row['BrandID']?>" <?=isset($product) && $product['BrandID']==$row['BrandID'] ? 'selected' : ''?>><?=$row['BrandName']?></option>
		        	<?php } ?>
		        </select>
		    </div>
		    <div class="form-group">
		        <label>Description</label>
	            <textarea name="product-desc" class="tinymce" placeholder="" rows="10"><?=isset($product) ? $product['ProductDescription'] : ''?></textarea>
		    </div>
		    <div class="form-group">
		        <label>Product Image: <strong>(Recommended dimension : 650 x 490)</strong></label>
		        <?php if(isset($product) && $product['Filename']!=""){ ?>
		        <br/>
		        <img src="<?=base_url('data/product/tmb/'.$product['Filename'])?>">
		        <?php } ?>
		        <input type="file" name="image" class="form-control">
		    </div>
		    <div class="form-group">
		        <label>Brosur PDF File : </label>
		        <?php if(isset($product) && $product['BrosurFilename']!=""){ ?>
		        <br/>
		        <a href="<?=base_url('produk/download/'.$product['BrosurFilename'])?>">Brosur File</a>
		        <span id="brosur-remove-btn">&nbsp; &middot; &nbsp;
			        <a class="js-post" data-url="<?=base_url('admin/product/deleteBrosur')?>" data-confirm="Are you sure want to delete this brosur file?" data-post='{"product-id":"<?=$product['ProductID']?>"}'>Delete file</a>
			    </span>
		        <?php } ?>
		        <input type="file" name="brosur" class="form-control">
		    </div>
		</div>
		<div class="col-md-4">
		    <label>SEO Description (Max. 150)</label>
		    <textarea name="product-seo-desc" class="form-control" maxlength="150"><?=isset($product) ? $product['SEODescription'] : ''?></textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		    <div class="form-group"><br/>
				<div class="alert alert-success alert-dismissable animated form-msg">
		            <i class="fa fa-check"></i>&nbsp;&nbsp;
		            <span></span>
		        </div>
		    </div>
	        <div class="box-footer">
	            <button type="submit" class="btn btn-primary btn-block" onclick="jquery_form('#add-product-form');"><?=$sub_menu?></button>
	        </div>
	    </div>
	</div>
</form>
<?php if(isset($product) && $product['Filename']!=""){ ?>
<script>
	function brosurRemoved(){
		$("#brosur-remove-btn").remove();
	}
</script>
<?php } ?>