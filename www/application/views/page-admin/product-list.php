<a href="<?=base_url('admin/product/add')?>"><div class="btn btn-sm btn-primary mb-10">Add a new product</div></a>
<table class="table table-responsive table-striped">
  <thead>
  <tr>
    <th width="80"></th>
    <th>Product Name</th>
    <th>Description</th>
    <th width="90"></th>
  </tr>
  </thead>
  <tbody>
    <?php foreach($product_list as $pro){?>
    <tr id="product-<?=$pro['ProductID']?>">
      <td>
        <a href="<?=base_url('produk/'.$pro['ProductID'].'/'.$this->function_model->url_no_space($pro['ProductName']))?>"><img src="<?=base_url()?>data/product/tmb/<?=$pro['Filename']?>" class="img-responsive"></a>
      </td>
      <td><a href="<?=base_url('produk/'.$pro['ProductID'].'/'.$this->function_model->url_no_space($pro['ProductName']))?>"><?=$pro['ProductName']?></a></td>
      <td><?=substr($pro['Description'],0,200)?></td>
      <td>
        <a id="show-product-<?=$pro['ProductID']?>" class="btn btn-xs product-btn btn-<?=$pro['Status'] == 1 ? 'success' : 'default'?> js-post"  data-url="<?=base_url('admin/product/show_product')?>" data-post='{"product-id":"<?=$pro['ProductID']?>"}'><i class="fa fa-check"></i></a>
        <!-- <button class="btn btn-<?=$pro['Status'] == 1 ? 'success' : 'default'?> btn-xs" id="show-product-<?=$pro['ProductID']?>" onclick="show_product(<?=$pro['ProductID']?>);"><i class="fa fa-check"></i></button> -->
        <a href="<?=base_url()?>admin/product/edit/<?=$pro['ProductID']?>"><button class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></button></a>
        <button class="btn btn-default btn-xs js-post"  data-url="<?=base_url('admin/product/delete')?>" data-confirm="Are you sure want to delete <?=$pro['ProductName']?>" data-post='{"product-id":"<?=$pro['ProductID']?>"}'><i class="fa fa-times"></i></button>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<br/>
<?php //$this->pagination->create_links();?>
<script>
  function productRemoved(response){
    if(response.status=="success"){ 
      $("#product-"+response.id).remove();
    }
  }
  function productActivation(response){
    if(response.status=="success"){
      if(response.message=="On"){
        $("#show-product-"+response.id).removeClass('btn-default');
        $("#show-product-"+response.id).addClass('btn-success');
      }else{
        $("#show-product-"+response.id).removeClass('btn-success');
        $("#show-product-"+response.id).addClass('btn-default');
      }
    }
  }
</script>