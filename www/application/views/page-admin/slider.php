<div class="row">
	<div class="col-md-7">
		<div class="box box-info table-responsive">
			<div class="box-header">
				<div class="box-title">Slider Images</div>
			</div>
			<div class="box-body no-padding">
	            <table class="table" id="slider-image-container">
	            	<thead>
		                <tr>
			                <th>Large</th>
			                <th>Small</th>
			                <th style="width: 120px;">Action</th>
		                </tr>
		            </thead>
	                <tbody>
		                <!-- <tr>
			                <td>
						        <form id="add-slider-img-form" method="post" action="<?=base_url()?>admin/slider/submit_slider">
						        	<input type="file" name="slider-file-img" onchange="add_slider_img();" id="slider-file-img">
						        </form>
						    </td>
			                <td></td>
		                </tr> -->
		                <?php foreach($slider_img as $row) {?>

			                <tr id="slider-img-<?=$row['SliderID']?>">
				                <td>
				                	<img src="<?=base_url()?>data/slider/<?=$row['Filename']?>" class="img-responsive">
				                	Link : <a href="<?=$row['Link']?>"><?=$row['Link']?></a>
				                </td>
				                <td><img src="<?=base_url()?>data/slider/small/<?=$row['SmallFilename']?>" class="img-responsive"></td>
				                <td>
				                	<i class="btn fa fa-angle-up btn-sm btn-default js-post" data-url="<?=base_url('admin/slider/reposition')?>" data-post='{"id":"<?=$row['SliderID']?>","type":"up"}'></i>
				                	<i class="btn fa fa-angle-down btn-sm btn-default js-post" data-url="<?=base_url('admin/slider/reposition')?>" data-post='{"id":"<?=$row['SliderID']?>","type":"down"}'></i>
				                	<i class="btn fa fa-trash-o btn-sm btn-danger js-post" data-url="<?=base_url('admin/slider/delete_slider')?>" data-post='{"id":"<?=$row['SliderID']?>"}' data-confirm="Are you sure want to delete this slider?"></i>
				                </td>
				            </tr>
			            <?php } ?>
		            </tbody>
	            </table>
			</div>
		</div>
	</div>
	<div class="col-md-5">
		<div class="box box-info">
			<div class="box-header">
				<div class="box-title">Slider Images</div>
			</div>
			<div class="box-body no-padding">

				<form id="add-slider-img-form" method="post" action="<?=base_url()?>admin/slider/submit_slider" enctype="multipart/form-data">
					<table class="table">
						<tr>
							<th>Large Slider (Recommended dimension : 1440 x 550)</th>
							<td>
								<input type="file" name="slider-file-img" class="form-msg form-reset">
								<small><i>supported file ext: .jpg, .png</i></small>
							</td>
						</tr>
						<tr>
							<th>Small Slider (Recommended dimension : 480 x 360)</th>
							<td>
								<input type="file" name="slider-file-img-small" class="form-msg form-reset">
								<small><i>supported file ext: .jpg, .png</i></small>
							</td>
						</tr>
						<tr>
							<th>Link</th>
							<td><input type="text" class="form-control form-reset" name="link"></td>
						</tr>
						<tr>
							<td colspan="2">
								<div class="alert alert-danger form-msg animated"><i class="fa fa-ban"></i> <span></span></div>
								<div class="text-right">
									<button class="btn btn-primary" onclick="jquery_form('#add-slider-img-form');">Add New Slider</button>
								</div>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	function sliderAdded(response){
		if(response.status=="success-reset"){
			$("#slider-image-container tbody").prepend(response.content);
		}
	}
	function sliderDeleted(response){
		if(response.status=="success"){
			$("#slider-img-"+response.id).remove();
		}
	}
	function moveSlider(response){
		if(response.type=="up"){
			var target_selector = $("#slider-img-"+response.id).next();
			$("#slider-img-"+response.id).insertAfter(target_selector);
		}
		else{
			var target_selector = $("#slider-img-"+response.id).prev();
			$("#slider-img-"+response.id).insertBefore(target_selector);
		}
	}
</script>