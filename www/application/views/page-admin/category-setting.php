<div class="col-md-3">
    <form action="<?=base_url('admin/category/submit')?>" enctype="multipart/form-data" method="post" id="add-category-form">
        <div class="box box-warning">
            <div class="box-header"><div class="box-title"><?=$edit['CategoryID']!=0 ? 'EDIT' : 'ADD' ?> CATEGORY</div></div>
            <div class="box-body">
                <input type="hidden" value="<?=$edit['CategoryID']!=0 ? $edit['CategoryID'] : 0 ?>" name="category-id">
                <label>Category Name</label>
                <input type="text" class="form-control mb-10" name="name" value="<?=$edit['CategoryID']!=0 ? $edit['CategoryName'] : '' ?>" placeholder="Your Category Title">
                <label>Category URL Title</label>
                <input type="text" class="form-control mb-10" name="keyname" value="<?=$edit['CategoryID']!=0 ? $edit['CategoryKeyname'] : '' ?>" placeholder="your-category-title">
                <label>Description</label>
                <textarea class="form-control mb-10" name="description" rows="5"><?=$edit['Description']!="" ? $edit['Description'] : '' ?></textarea>
                <label>Brand</label>
                <select class="form-control mb-10" name="brand[]" multiple>
                    <?php foreach($brand as $row) {?>
                    <option value="<?=$row['BrandID']?>"><?=$row['BrandName']?></option>
                    <?php } ?>
                </select>
                <label>Category Image</label>
                <br>
                <small>Recomended size: 150x150 pixel</small>
                <input type="file" name="image" class="form-control mb-10">
                <div class="alert alert-danger animated form-msg">
                    <i class="fa fa-ban"></i>
                    <span></span>
                </div>
                <input type="submit" class="btn mb-10 btn-success btn-block" value="<?=$edit!=0 ? 'EDIT' : 'ADD' ?> CATEGORY" onclick="jquery_form('#add-category-form');">
                <?php if($edit!=0){ ?>
                <a href="<?=base_url('admin/category')?>"><input type="button" class="btn btn-warning btn-block" value="CANCEL"></a>
                <?php } ?>
            </div>
        </div>
    </form>
</div>
<div class="col-md-9">
    <div class="box box-info">
        <table class="table">
            <thead>
                <tr>
                    <th width="100"></th>
                    <th>Category Name</th>
                    <th>Category Keyname</th>
                    <th>Description</th>
                    <th>Brands</th>
                    <th width="150"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($parent as $row) {?>
                <tr id="category-<?=$row['CategoryID']?>">
                    <td>
                        <?php if($row['Filename']!="") {?>
                        <img src="<?=base_url('data/category/'.$row['Filename'])?>" class="img-responsive">
                        <?php } ?>
                    </td>
                    <td><?=$row['CategoryName']?></td>
                    <td><?=$row['CategoryKeyname']?></td>
                    <td><?=$row['Description']?></td>
                    <td>
                        <?php
                            foreach($categoryBrand[$row['CategoryID']] as $catBrand){
                                echo "- ".$catBrand['BrandName']."<br>";
                            }
                        ?>
                    </td>
                    <td>
                        <i class="btn fa fa-angle-up btn-sm btn-default js-post" data-url="<?=base_url('admin/category/reposition')?>" data-post='{"id":"<?=$row['CategoryID']?>","type":"down"}'></i>
                        <i class="btn fa fa-angle-down btn-sm btn-default js-post" data-url="<?=base_url('admin/category/reposition')?>" data-post='{"id":"<?=$row['CategoryID']?>","type":"up"}'></i>
                        <a href="<?=base_url('admin/category?e='.$row['CategoryID'])?>"><i class="fa fa-edit btn btn-sm btn-info" ></i></button></a>
                        <i class="btn fa fa-trash-o btn-sm btn-danger js-post" data-url="<?=base_url('admin/category/delete')?>" data-post='{"id":"<?=$row['CategoryID']?>"}' data-confirm="Are you sure want to delete this category?"></i>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>


</div>
<script>
    function moveCategory(response){
        if(response.type=="up"){
            var target_selector = $("#category-"+response.id).next();
            $("#category-"+response.id).insertAfter(target_selector);
        }
        else{
            var target_selector = $("#category-"+response.id).prev();
            $("#category-"+response.id).insertBefore(target_selector);
        }
    }
</script>