<div id="product-img-modal" class="modal modal-vcenter fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Product Images</h4>
      </div>
      <div class="modal-body clearfix">
        <?php foreach($product_img as $row){?>
        <div class="col-sm-4 no-padding">
            <form action="<?=base_url('admin/product/change_variant_img')?>" method="post" id="variant-image-form-<?=$row['ImageID']?>">
                <input type="hidden" name="image-id" value="<?=$row['ImageID']?>" class="variant-image-image-id">
                <input type="hidden" name="variant-id" value="<?=$row['ImageID']?>" class="variant-image-variant-id">
                <img onclick="submit_form('#variant-image-form-<?=$row['ImageID']?>');" src="<?=base_url('data/product/tmb/'.$row['Filename'])?>">
            </form>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div id="edit-variant-modal" class="modal modal-vcenter fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    <form action="<?=base_url('admin/product/submit_variant')?>" method="post" id="edit-variant-form">
      <div class="modal-body clearfix">  
        <input type="hidden" name="variant-id">
        <small>Product Name</small><br/>
        <b id="edit-variant-product-name"></b> (<span id="edit-variant-variant-string"></span>)<br/>
        <small>Price</small><br/>
        <input type="text" class="form-control" name="price">
        <div class="row">
            <div class="col-md-6">
                <small>Stock</small><br/>
                <input type="text" class="form-control" name="stock">
            </div>
            <div class="col-md-6">
                <small>Weight</small><br/>
                <input type="text" class="form-control" name="weight">
            </div>
        </div>
      </div>
      <div class="modal-footer text-right">
        <button class="btn btn-primary" onclick="submit_form('#edit-variant-form'); return false;">Edit Variant Item</button>
      </div>
    </form>
    </div>
  </div>
</div>
<div class="row">
	<div class="col-md-7">
        <div id="product-detail-image">
    		<img src="<?=base_url()?>data/product/<?=$product_desc['Filename']?>" class="img-responsive">
        </div><br/>
        <div id="product-img-container">
            <?php foreach($product_img as $row){?>
            <div class="col-sm-2 col-xs-3 product-img-item" id="product-img-<?=$row['ImageID']?>">
                <img src="<?=base_url()?>data/product/tmb/<?=$row['Filename']?>" data-url="<?=base_url()?>data/product/<?=$row['Filename']?>" class="other-product-img img-responsive">
                <div class="product-img-button">
                    <div class="button-container">
                        <a class="btn btn-xs" onclick="set_as_primary(<?=$row['ImageID']?>,<?=$row['ProductID']?>);"><i class="fa fa-star"></i></a>
                        <a class="btn btn-xs" onclick="delete_product_img(<?=$row['ImageID']?>);"><i class="fa fa-trash-o"></i></a>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div><br/>

        <div class="content-box">
            <div class="content-box-body">
                <h4>
                    Images 
                    <div class="btn btn-info mb-10 pull-right" onclick="uploadDropzone();"><i class="fa fa-upload"></i> Upload Images</div>
                </h4>
                <div class="dropzone" id="dropzone">
                    <input type="file" name="file">
                    <input type="hidden" name="product-id" id="product-id" value="<?=$product_desc['ProductID']?>">
                </div>
            </div>
        </div>
	</div>
	<div class="col-md-5">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Product Info </h3>
                <div class="pull-right box-tools">
                    <button class="btn btn-<?=$product_desc['Status'] == 1 ? 'success' : 'default'?> btn-xs" title="Show Product : On" id="show-product-<?=$product_desc['ProductID']?>" onclick="show_product(<?=$product_desc['ProductID']?>);"><i class="fa fa-check"></i></button>
                    <a href="<?=base_url()?>admin/product/edit/<?=$product_desc['ProductID']?>" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></a>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-condensed">
                    <tr>
                        <th style="width: 150px;">Product Name</th>
                        <td><?=$product_desc['ProductName']?></td>
                    </tr>
                    <tr>
                        <th style="width: 150px;">Description</th>
                        <td><?=$this->ryan_editor_model->editor_converter(nl2br($product_desc['ProductDescription']))?></td>
                    </tr>
                    <tr>
                        <th style="width: 150px;">Stock</th>
                        <td>
                            <?php
                                if(count($option)>0){
                                    if($option[0]['Option']=="notset"){
                                        echo ($option[0]['Stock']==99999999 ? 'unlimited stock' : $option[0]['Stock']);
                                    }
                                    else{
                                        foreach($option as $row){
                                            echo $row['Option']." [".($row['Stock']==99999999 ? 'unlimited stock' : $row['Stock'])."]<br/>";
                                        }
                                    }
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Price</th>
                        <td><?=$f->price_format($product_desc['NormalPrice'])?></td>
                    </tr>
                </table>
            </div><!-- /.box-body -->
        </div>
		
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Volume Discount</h3>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-condensed text-center">
                    <tr>
                        <th width="50%">%</th>
                        <th>Minimum</th>
                    </tr>
                    <?php foreach($volume_discount as $row) {?>
                    <tr>
                        <td><?=$row['Value']?>%</td>
                        <td><?=$row['Minimum']?></td>
                    </tr>
                    <?php } ?>
                </table>
            </div><!-- /.box-body -->
        </div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Product Variants </h3>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-condensed">
                    <tr>
                        <th width="95"></th>
                        <th>Variants</th>
                        <th width="100">Price</th>
                        <th width="50" class="text-center">Stock</th>
                        <th width="70" class="text-center">Weight</th>
                        <th width="70"></th>
                    </tr>
                    <?php foreach($variants as $row) {?>
                    <tr>
                        <td>
                            <?php ?>
                            <img id="variant-img-<?=$row['VariantID']?>" width="80" onclick="changeVariantImg(<?=$row['VariantID']?>);" src="<?=base_url('data/product/tmb')?>/<?=$row['Filename']!="" ? $row['Filename'] : $product_desc['Filename']?>">
                        </td>
                        <td>
                            <?php echo $row['Variant1'] ?>
                            <?php echo $row['Variant2'] != "" ? " &middot; ".$row['Variant2'] : '' ?>
                            <?php echo $row['Variant3'] != "" ? " &middot; ".$row['Variant3'] : '' ?>
                        </td>
                        <td id="variant-item-price-<?=$row['VariantID']?>">
                            <?php echo $this->function_model->price_format($row['VariantPrice']) ?>
                        </td>
                        <td class="text-center" id="variant-item-stock-<?=$row['VariantID']?>"><?php echo $row['VariantStock'] ?></td>
                        <td class="text-center" id="variant-item-weight-<?=$row['VariantID']?>"><?php echo $row['VariantWeight'] ?></td>
                        <td class="text-right">
                            <div class="btn btn-xs btn-default" onclick="editVariantItem(<?=$row['VariantID']?>,'<?=$row['ProductName']?>','<?php echo $row['Variant1'] ?><?php echo $row['Variant2'] != "" ? " &middot; ".$row['Variant2'] : '' ?><?php echo $row['Variant3'] != "" ? " &middot; ".$row['Variant3'] : '' ?>');"><i class="fa-pencil fa"></i></div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div><!-- /.box-body -->
        </div>
	</div>
</div>
<script>
    function variantImageChanged(result){
        var base_url = $("#base_url").attr('href');
        var img_url = base_url+"data/product/tmb/"+result.message;
        $("#variant-img-"+result.id).attr('src',img_url);
        $("#product-img-modal").modal('hide');
    }
    function changeVariantImg(id){
        $(".variant-image-variant-id").val(id);
        $("#product-img-modal").modal('show');
    }
    function editVariantItem(id,product_name,variant_string){

        var price = $("#variant-item-price-"+id).html();
        price = unconvert_currency(price.replace('IDR ',''));
        var weight = $("#variant-item-weight-"+id).html();
        var stock = $("#variant-item-stock-"+id).html();

        $("#edit-variant-form input[name='variant-id']").val(id);
        $("#edit-variant-form input[name='price']").val(price);
        $("#edit-variant-form input[name='weight']").val(weight);
        $("#edit-variant-form input[name='stock']").val(stock);
        $("#edit-variant-product-name").html(product_name);
        $("#edit-variant-variant-string").html(variant_string);
        $("#edit-variant-modal").modal('show');
    }

    function variantEdited(result){
        $("#variant-item-price-"+result.data.VariantID).html('IDR '+convert_to_currency_format(result.data.VariantPrice));
        $("#variant-item-stock-"+result.data.VariantID).html(result.data.VariantStock);
        $("#variant-item-weight-"+result.data.VariantID).html(result.data.VariantWeight);
        $("#edit-variant-modal").modal('hide');
    }
</script>