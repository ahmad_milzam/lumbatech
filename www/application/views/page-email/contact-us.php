<!-- Inliner Build Version 4380b7741bb759d6cb997545f3add21ad48f010b -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width" />
</head>
<body style="width: 100% !important; min-width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; margin: 0; padding: 0;">
  <style type="text/css">
    a:hover {
    color: #2795b6 !important;
    }
    a:active {
    color: #2795b6 !important;
    }
    a:visited {
    color: #2ba6cb !important;
    }
    h1 a:active {
    color: #2ba6cb !important;
    }
    h2 a:active {
    color: #2ba6cb !important;
    }
    h3 a:active {
    color: #2ba6cb !important;
    }
    h4 a:active {
    color: #2ba6cb !important;
    }
    h5 a:active {
    color: #2ba6cb !important;
    }
    h6 a:active {
    color: #2ba6cb !important;
    }
    h1 a:visited {
    color: #2ba6cb !important;
    }
    h2 a:visited {
    color: #2ba6cb !important;
    }
    h3 a:visited {
    color: #2ba6cb !important;
    }
    h4 a:visited {
    color: #2ba6cb !important;
    }
    h5 a:visited {
    color: #2ba6cb !important;
    }
    h6 a:visited {
    color: #2ba6cb !important;
    }
    table.button:hover td {
    background: #2795b6 !important;
    }
    table.button:visited td {
    background: #2795b6 !important;
    }
    table.button:active td {
    background: #2795b6 !important;
    }
    table.button:hover td a {
    color: #fff !important;
    }
    table.button:visited td a {
    color: #fff !important;
    }
    table.button:active td a {
    color: #fff !important;
    }
    table.button:hover td {
    background: #2795b6 !important;
    }
    table.tiny-button:hover td {
    background: #2795b6 !important;
    }
    table.small-button:hover td {
    background: #2795b6 !important;
    }
    table.medium-button:hover td {
    background: #2795b6 !important;
    }
    table.large-button:hover td {
    background: #2795b6 !important;
    }
    table.button:hover td a {
    color: #ffffff !important;
    }
    table.button:active td a {
    color: #ffffff !important;
    }
    table.button td a:visited {
    color: #ffffff !important;
    }
    table.tiny-button:hover td a {
    color: #ffffff !important;
    }
    table.tiny-button:active td a {
    color: #ffffff !important;
    }
    table.tiny-button td a:visited {
    color: #ffffff !important;
    }
    table.small-button:hover td a {
    color: #ffffff !important;
    }
    table.small-button:active td a {
    color: #ffffff !important;
    }
    table.small-button td a:visited {
    color: #ffffff !important;
    }
    table.medium-button:hover td a {
    color: #ffffff !important;
    }
    table.medium-button:active td a {
    color: #ffffff !important;
    }
    table.medium-button td a:visited {
    color: #ffffff !important;
    }
    table.large-button:hover td a {
    color: #ffffff !important;
    }
    table.large-button:active td a {
    color: #ffffff !important;
    }
    table.large-button td a:visited {
    color: #ffffff !important;
    }
    table.secondary:hover td {
    background: #d0d0d0 !important; color: #555;
    }
    table.secondary:hover td a {
    color: #555 !important;
    }
    table.secondary td a:visited {
    color: #555 !important;
    }
    table.secondary:active td a {
    color: #555 !important;
    }
    table.success:hover td {
    background: #457a1a !important;
    }
    table.alert:hover td {
    background: #970b0e !important;
    }
  </style>

  <table class="body" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;">
    <tr style="vertical-align: top; text-align: left; padding: 0;" align="left">
      <td class="center" align="center" valign="top" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;">
        <center style="width: 100%; min-width: 580px;">

        <!-- Email Content -->

          <!-- container -->
          <table class="container" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; width: 580px; margin: 0 auto; padding: 0;">
            <tr style="vertical-align: top; text-align: left; padding: 0;" align="left">
              <td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top">

                <!-- table row: Greeting -->
                <table class="row" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0px;">
                  <tr style="vertical-align: top; text-align: left; padding: 0;" align="left">
                    <td class="wrapper last" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 10px 0px 0px;" align="left" valign="top">

                      <table class="twelve columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;">
                        <tr style="vertical-align: top; text-align: left; padding: 0;" align="left">
                          <td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0px 0px 10px;" align="left" valign="top">

                            <h5 style="color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 1.3; word-break: normal; font-size: 24px; margin: 0; padding: 0;" align="left">
                                Anda mendapatkan pesan
                            </h5>
                            <br />
                            <p style="color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; margin: 0 0 10px; padding: 0;" align="left">
                              <strong>Dari:</strong> <?=$nama?>
                              <br />
                              <strong>Email:</strong> <?=$email?>
                              <br />
                              <strong>No. Telepon</strong> : <?=$telepon?>

                              <!-- kalo ada input alamat -->
                              <?php if(isset($alamat) && $alamat!="") {?>
                              <br />
                              <strong>Alamat:</strong>
                              <br />
                              <?=nl2br($alamat)?>
                              <br />
                              <?php } ?>
                              <!-- kalo ada input alamat -->

                              <!-- kalo ada barang yang di pilih -->
                              <?php if(isset($nama_produk) && $nama_produk) {?>
                              <br />
                              <strong>Nama Produk:</strong> <?=$nama_produk?>
                              <br />
                              <strong>Jumlah:</strong> <?=$jumlah_produk?>
                              <?php } ?>
                              <!-- kalo ada barang yang di pilih -->
                            </p>

                            <p style="color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 19px; font-size: 14px; margin: 0 0 10px; padding: 0;" align="left">
                              <?=nl2br($pesan)?>
                            </p>

                          </td>
                          <td class="expander" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; color: #222222; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;" align="left" valign="top"></td>
                        </tr>
                      </table>

                    </td>
                  </tr>
                </table><!-- /table row: Greeting -->

              </td>
            </tr>
          </table><!-- /container -->
        <!-- Email Content -->
        </center>

      </td>
    </tr>
  </table>
</body>
</html>
