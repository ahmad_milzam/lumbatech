<?php
	class Tentang extends CI_Controller{
		function __construct(){
			parent::__construct();
		}

		function index(){
			$data['content'] = "tentang";
			$this->load->model('info_model');
			$data['text'] = $this->info_model->get_info_page('tentang');
			$data['breadcrumbs'] = array("Home"=>base_url(),"Tentang"=>"#");
			$data['web_title'] = "Tentang Kami";
			$this->load->view('page-user/mainpage',$data);
		}
	}
?>	