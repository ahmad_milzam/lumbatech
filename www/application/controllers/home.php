<?php
	class Home extends CI_Controller{
		function __construct(){
			parent::__construct();
		}

		function index(){
			$data['content'] = "home";
			$this->load->model('slider_model');
			$this->load->model('category_model');
			$data['slider'] = $this->slider_model->get_img_slider();
			$data['category'] = $this->category_model->get_category();
			$this->load->view('page-user/mainpage',$data);
		}
	}
