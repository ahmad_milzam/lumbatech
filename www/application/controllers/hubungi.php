<?php
	class Hubungi extends CI_Controller{
		function __construct(){
			parent::__construct();
		}

		function index($id = ""){
			$this->load->model('product_model');
			$data['product'] = $this->product_model->get_product(array("ProductID"=>$id));
			$data['content'] = "hubungi";
			$data['breadcrumbs'] = array("Home"=>base_url(),"Hubungi"=>"#");
			$data['web_title'] = "Hubungi Kami";
			$this->load->view('page-user/mainpage',$data);
		}
		function kirim(){
			$this->load->model('contact_model');
			echo json_encode($this->contact_model->send());
		}
		function test(){
			$generalSetting = $this->website_model->get_website_setting();
			print_r($generalSetting);
		}
	}
?>	