<?php
	class Produk extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('category_model');
			$this->load->model('product_model');
		}

		function index($category_name = "all",$brand_name = "all", $page = 1){
			$limit = 15;
			$data['current_category_keyname'] = $category_name;
			$data['content'] = "produk";
			$data['category'] = $this->category_model->getCategory();

			if(count($data['category'])>0){
				foreach($data['category'] as $row){
					$data['categoryBrand'][$row['CategoryID']] = $this->category_model->get_category_brand(array("CategoryID"=>$row['CategoryID']));
					//$data['child'][$row['CategoryID']] = $this->category_model->get_category($row['CategoryID']);
				}
			}
			if($category_name!="all"){
				$category_detail = $this->category_model->getCategory(array("CategoryKeyname"=>$category_name));
				$data['breadcrumbs'] = array("Home"=>base_url(),"Produk"=>base_url('produk/all'),$category_detail['CategoryName']=>base_url('produk/'.$category_detail['CategoryKeyname']));
				$data['web_title'] = "Produk ".$category_detail['CategoryName'];
			}
			else{
				$data['web_title'] = "Semua Produk";
				$data['breadcrumbs'] = array("Home"=>base_url(),"Produk"=>base_url('produk/all'),"All"=>"#");
			}
			if($brand_name!="all"){
				$this->load->model('brand_model');
				$brand_detail = $this->brand_model->get_brand(array("BrandKeyname"=>$brand_name));
				$data['breadcrumbs'] = array_merge($data['breadcrumbs'],array($brand_detail['BrandName']=>"#"));
				$data['web_title'] .= " Merek ".$brand_detail['BrandName'];
			}
			$data['product'] = $this->product_model->get_product(array("CategoryKeyname"=>$category_name,"BrandKeyname"=>$brand_name,"limit"=>$limit,"page"=>$page));

			$config = array('base_url'=>base_url().'produk/'.$category_name.'/'.$brand_name,
							'total_rows'=>count($this->product_model->get_product(array("CategoryKeyname"=>$category_name,"BrandKeyname"=>$brand_name))),
							'num_links'=>3,
							'per_page'=>$limit,'uri_segment'=>4,
							'use_page_numbers'=>TRUE,
							);
			$this->load->model('pagination_model');
			$config_paging = $this->pagination_model->pagination(1);
			$config = array_merge($config,$config_paging);
			$this->load->library('pagination');
			$this->pagination->initialize($config);

			$this->load->view('page-user/mainpage',$data);
		}
		function detail($id = "", $product_name = ""){
			$data['product'] = $this->product_model->get_product(array("ProductID"=>$id));
			$data['category'] = $this->category_model->getCategory();
			if(count($data['category'])>0){
				foreach($data['category'] as $row){
					$data['categoryBrand'][$row['CategoryID']] = $this->category_model->get_category_brand(array("CategoryID"=>$row['CategoryID']));
					//$data['child'][$row['CategoryID']] = $this->category_model->get_category($row['CategoryID']);
				}
			}
			if(count($data['product'])>0){
				$data['web_title'] = $data['product']['ProductName'];
				$data['og_img'] = base_url('data/product/'.$data['product']['Filename']);
				$data['content'] = "produk-detail";
				$data['meta_description'] = $data['product']['SEODescription'];
				$data['breadcrumbs'] = array("Home"=>base_url(),"Produk"=>base_url('produk'),$data['product']['CategoryName']=>base_url('produk/'.$data['product']['CategoryKeyname']),$data['product']['BrandName']=>base_url('produk/'.$data['product']['CategoryKeyname'].'/'.$data['product']['BrandKeyname']),$data['product']['ProductName']=>"#");
				$this->load->view('page-user/mainpage',$data);
			}
			else{
				show_404();
			}
		}
		function download($filename = ''){

			if($filename!=""){
				$path = './data/file/';

				$file = $path.$filename;
				header("Content-Type: application/octet-stream");
				header("Content-Type: application/download");
				header("Content-Description: File Transfer");
				header("Content-Disposition: attachment; filename=" . urlencode($filename));
				header("Content-Length: " . filesize($file));

				flush(); // this doesn't really matter.

				$fp = fopen($file, "r");
				while (!feof($fp))
				{
				  echo fread($fp, 65536);
				   flush(); // this is essential for large downloads
				}
				fclose($fp);
			}
		}
	}
?>