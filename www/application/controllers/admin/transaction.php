<?php
	class Transaction extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('transaction_model');
		}
		function lists($page = 1){
			$limit = 16;
			$date = date("m/01/Y - m/d/Y");
			if($this->input->get('d')){
				$date = $this->input->get('d');	

			}
			$data['f'] = $this->function_model;
			$data['filter_date'] = $date;
			$data['content'] = "transaction-list";
			$data['sub_menu'] = "Transaction List";
			$data['transaction_list'] = $this->transaction_model->get_user_transaction('all',$limit, $page, $date);
			$config = array('base_url'=>base_url().'admin/transaction',
							'total_rows'=>count($this->transaction_model->get_user_transaction('all',1, 'all', $date)),
							'num_links'=>2,
							'per_page'=>$limit,'uri_segment'=>3,
							'use_page_numbers'=>TRUE,
							);
			if($date!='all'){
				$config['suffix'] = '/?d='.$date;
        		$config['first_url'] = base_url().'admin/transaction/1'.$config['suffix']; 
			}
			$this->load->model('pagination_model');
			$config_paging = $this->pagination_model->pagination(1);
			$config = array_merge($config,$config_paging);
			$this->pagination->initialize($config);
			$this->load->view('admin',$data);
		}
		function detail($id = ""){
			
			$data['content'] = "transaction-detail";
			$data['sub_menu'] = "Transaction Detail";
			$data['transaction_detail'] = $this->transaction_model->getTransactionDetail(array("TransactionID"=>$id));
			if(count($data['transaction_detail'])>0){
				$data['f'] = $this->function_model;
				$data['trans_header'] = $this->transaction_model->get_transaction_header($id);
				$data['confirmation_history'] = $this->transaction_model->get_confirmation_history($id);
			}
			else{
				show_404();
			}

			$this->load->view('admin',$data);
		}
		function process(){
			if($this->session->userdata('admin')){
				echo $this->transaction_model->process();						
			}
		}
		function save_airbill(){
			if($this->session->userdata('admin')){
				echo $this->transaction_model->save_airbill();						
			}
		}
		function edit_shipping_fee(){
			if($this->session->userdata('admin')){
				echo $this->transaction_model->edit_shipping_fee();
			}	
		}
	}
?>