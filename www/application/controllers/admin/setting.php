<?php
	class Setting extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('website_model');
		}
		function index(){
			$data['content'] = "website-setting";
			$data['sub_menu'] = "Website Setting";
			$data['setting'] = $this->website_model->get_website_setting();
			$data['js'] = array("tinymce/tinymce");
			$this->load->view('page-admin/mainpage',$data);
		}
		function submit_setting(){

			if($this->session->userdata('admin')){
				echo $this->website_model->submit_setting();

			}
		}
		function upload_logo(){
			if($this->session->userdata('admin')){
				echo $this->website_model->upload_logo();
			}
		}
		function upload_header_logo(){
			if($this->session->userdata('admin')){
				echo $this->website_model->upload_header_logo();
			}
		}
	}
?>