<?php
	class Product extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('product_model');
		}
		function lists($page = 1){
			$limit = 16;
			$data['f'] = $this->function_model;

			// $config = array('base_url'=>base_url().'admin/product',
			// 				'total_rows'=>count($this->product_model->get_product('all','all', 
			// 					1,'','all')),
			// 				'num_links'=>2,
			// 				'per_page'=>$limit,'uri_segment'=>3,
			// 				'use_page_numbers'=>TRUE,
			// 				);
			// $this->load->model('pagination_model');
			// $config_paging = $this->pagination_model->pagination(1);
			// $config = array_merge($config,$config_paging);
			// $this->pagination->initialize($config);
			$data['content'] = "product-list";
			$data['sub_menu'] = "Product List";
			// $data['product_list'] = $this->product_model->get_product('all',$limit,$page,'','all');
			$data['product_list'] = $this->product_model->get_product();
			$this->load->view('page-admin/mainpage',$data);
		}

		function detail($id){
			$data['product_desc'] = $this->product_model->get_product_detail($id,'all');
			$data['f'] = $this->function_model;
			$data['js'] = array('dropzone','../../js/currency');
			$data['css'] = array('dropzone');
			$this->load->model('ryan_editor_model');
			//$data['product_detail_list'] = $this->product_model->get_product_detail_list($id);
			$data['product_img'] = $this->product_model->get_product_img($id);
			$data['option'] = $this->product_model->get_product_option($id);
			$data['variants'] = $this->product_model->getProductVariant(array("product_id"=>$id));
			$data['volume_discount'] = $this->product_model->get_volume_discount(array("ProductID"=>$id));

			$data['content'] = "product-detail";
			$data['sub_menu'] = "Product Detail";
			$data['editor'] = $this->ryan_editor_model;
    		$this->load->view('page-admin/mainpage',$data);
		}
		function add(){
			$this->load->model('category_model');
			$this->load->model('brand_model');
			$data['category'] = $this->category_model->get_category();
			$data['brand'] = $this->brand_model->get_brand();
			$data['js'] = array("tinymce/tinymce");
			$data['content'] = 'add-product';
			$data['sub_menu'] = "Add Product";
    		$this->load->view('page-admin/mainpage',$data);
		}
		function edit($id = ''){
			$this->load->model('category_model');
			$this->load->model('brand_model');
			$data['content'] = 'add-product';
			$data['sub_menu'] = "Edit Product";
			$data['js'] = array("tinymce/tinymce");
			if($id!=""){
				$data['product'] = $this->product_model->get_product(array("ProductID"=>$id));
				if(count($data['product'])>0){
					$data['category'] = $this->category_model->get_category();
					$data['brand'] = $this->brand_model->get_brand();
    				$this->load->view('page-admin/mainpage',$data);
				}

			}
		}
		function submit(){
			if($this->session->userdata('admin')){
				echo $this->product_model->submit();
			}
		}
		function show_product(){
			if($this->session->userdata('admin')){
				echo $this->product_model->show_product();
			}
		}
		function delete(){
			if($this->session->userdata('admin')){
				echo $this->product_model->delete();
			}
		}
		function deleteBrosur(){
			if($this->session->userdata('admin')){
				echo $this->product_model->deleteBrosur();
			}
		}
		function submit_product_img(){
			if($this->session->userdata('admin')){
				$this->product_model->submit_product_img();
			}
		}
		function delete_product_img(){
			if($this->session->userdata('admin')){
				$this->product_model->delete_product_img();
			}
		}
		function primary_product_img(){
			if($this->session->userdata('admin')){
				$this->product_model->primary_product_img();
			}
		}
		function submit_variant(){
			if($this->session->userdata('admin')){
				$this->product_model->submit_variant();
			}
		}
		function change_variant_img(){
			if($this->session->userdata('admin')){
				$this->product_model->change_variant_img();
			}
		}

	}
?>