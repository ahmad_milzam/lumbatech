<?php
	class Info extends CI_Controller{
		function __construct(){
			parent::__construct();
		}
		function index(){
			$this->load->model('info_model');
			$data['content'] = "info";
			$data['sub_menu'] = "Info Help";
			$data['js'] = array("tinymce/tinymce");
			$data['info'] = $this->info_model->get_info_page();
			$this->load->view('page-admin/mainpage',$data);
		}
		function submit(){
			if($this->session->userdata('admin')){
				$this->load->model('info_model');
				$this->info_model->submit_info();
			}
		}
	}
?>