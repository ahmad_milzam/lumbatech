<?php
	class Dashboard extends CI_Controller{
		function __construct(){
			parent::__construct();
		}
		function index(){
			$data['content'] = "home";
			$data['sub_menu'] = "Dashboard";
			$this->load->view('page-admin/mainpage',$data);
		}
		function changePassword(){
			if($this->session->userdata('admin')){
				$this->load->model('admin_model');
				echo json_encode($this->admin_model->change_password());
			}
		}
	}
?>