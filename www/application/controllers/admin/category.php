<?php
	class Category extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('category_model');
		}
		function index(){
			$this->load->model('brand_model');

			$data['content'] = "category-setting";
			$data['sub_menu'] = "Category Settings";
			$data['parent'] = $this->category_model->get_category(0);
			$data['brand'] = $this->brand_model->get_brand(array('order_by'=>'BrandName'));
			$data['js'] = array("category");
			$data['edit'] = 0;
			if($this->input->get('e')){
				$data['edit'] = $this->input->get('e');
				$data['edit'] = $this->category_model->get_category_by_id($data['edit']);
			}
			
			if(count($data['parent'])>0){
				foreach($data['parent'] as $row){
					$data['categoryBrand'][$row['CategoryID']] = $this->category_model->get_category_brand(array("CategoryID"=>$row['CategoryID']));
					//$data['child'][$row['CategoryID']] = $this->category_model->get_category($row['CategoryID']);
				}
			}

			$this->load->view('page-admin/mainpage',$data);
		}
		function submit(){
			if($this->session->userdata('admin')){
				echo $this->category_model->submit();
			}
		}
		function delete(){
			if($this->session->userdata('admin')){
				echo $this->category_model->delete();
			}
		}
		function reposition(){
			if($this->session->userdata('admin')){
				$this->category_model->reposition();
			}
		}
	}
?>