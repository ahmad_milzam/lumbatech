<?php
	class Slider extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('slider_model');
		}
		function index(){
			$this->load->model('slider_model');
			$data['content'] = "slider";
			$data['sub_menu'] = "Slider";
			$data['slider_img'] = $this->slider_model->get_img_slider();
			$this->load->view('page-admin/mainpage',$data);
		}
		function submit_slider(){
			if($this->session->userdata('admin')){
				$this->slider_model->submit_slider();
			}
		}
		function delete_slider(){
			if($this->session->userdata('admin')){
				$this->slider_model->delete_slider();
			}
		}
		function reposition(){
			if($this->session->userdata('admin')){
				$this->slider_model->reposition();
			}
		}

	}
?>