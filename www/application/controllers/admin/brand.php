<?php
	class Brand extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('brand_model');
		}
		function index(){
			$data['content'] = "brand-setting";
			$data['sub_menu'] = "Brand Settings";
			$data['brand'] = $this->brand_model->get_brand();
			$data['js'] = array("category");
			$data['edit'] = 0;
			if($this->input->get('e')){
				$data['edit'] = $this->input->get('e');
				$data['edit'] = $this->brand_model->get_brand(array("BrandID"=>$data['edit']));
			}

			$keyname_check = $this->brand_model->get_brand(array("BrandKeyname"=>"samsung","not_BrandID"=>1));
			$this->load->view('page-admin/mainpage',$data);
		}
		function submit(){
			if($this->session->userdata('admin')){
				echo $this->brand_model->submit();
			}
		}
		function delete(){
			if($this->session->userdata('admin')){
				echo $this->brand_model->delete();
			}
		}
	}
?>