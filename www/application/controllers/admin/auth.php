<?php
	class Auth extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('category_model');
		}
		function login(){
			if(!$this->session->userdata('admin')){
				$f = $this->function_model;

				$username = $f->clean_input($this->input->post('username'));
				$password = $f->clean_input($this->input->post('password'));

				if($username&&$password){
					$this->db->from('msadmin');
					$this->db->where('username',$username);
					$this->db->where('password',sha1($password));
					$admin = $this->db->get()->row_array();
					if(count($admin)>0 && $admin['admin_id']!=""){
						$this->session->set_userdata('admin',$admin);
					}
				}
			}
			redirect(base_url().'admin');
		}

	}
?>