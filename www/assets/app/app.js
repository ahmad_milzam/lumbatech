function alert_div(elmnt, type){
	if(type=='success'){
		$(elmnt).removeClass('alert--error');
		$(elmnt).addClass('alert--success');	
		$(elmnt+" i").removeClass('fa-ban');	
		$(elmnt+" i").addClass('fa-check');	
	}
	else if(type=='error'){
		$(elmnt).removeClass('alert--success');	
		$(elmnt).addClass('alert--error');
		$(elmnt+" i").removeClass('fa-check');	
		$(elmnt+" i").addClass('fa-ban');

	}
}
function ajaxSend(url, data, loading_bool){
     loading(loading_bool);
     $.ajax({type:"POST", data: data, url: url}).done(function(result){
        
        loading(false);
        var result = $.parseJSON(result);
        if(typeof result.callFunction!="undefined" && result.callFunction!=""){
          window[result.callFunction](result);
        }

        if(typeof result.url!="undefined" && result.url!=""){
          if(result.url=="self")
            setTimeout(function(){location.reload();},1000);
          else if(result.url!="")
            window.location.href = result.url;
        }
     });
 }

function show_popup(){
	$("#popup").fadeIn(200);
	$("#popup-container").removeClass('bounceOutUp');
	$("#popup-container").addClass('bounceInDown');
}
function close_popup(){
	$("#popup").fadeOut(200);
	$("#popup-container").removeClass('bounceInDown');
}

function submit_form(form){ 
     var url = $(form).attr('action');
     $("#loading-overlay").fadeIn(200);
     $(form+" .form-msg").removeClass('shake');
     $(form+" .form-msg").removeClass('bounce');
     $.ajax({type:"POST", data: $(form).serialize(), url: url}).done(function(result){
     	$("#loading-overlay").fadeOut(200);
     	$(form+" .form-msg").show(0);
       	var result = $.parseJSON(result);
        alert_div(form+' .form-msg',result.status);
        if(result.status=='error'){
 			$(form+" .form-msg").addClass('shake');
        }
        else{
        	$(form+" .form-msg").addClass('bounce');
        }
        if(typeof result.callFunction!="undefined" && result.callFunction!=""){

          window[result.callFunction](result);
        }
        $(form+" .form-msg p").html(result.message);
     });
}

function loading(type){
	if(type==true){
     $("#loading").fadeIn(100);
 	}
 	else{
 	 $("#loading").fadeOut(100);
 	}
}