function alert_div(elmnt, type){
  if(type.indexOf('success')!=-1){
    $(elmnt).removeClass('alert-danger');
    $(elmnt).addClass('alert-success'); 
    $(elmnt+" i").removeClass('fa-ban');  
    $(elmnt+" i").addClass('fa-check'); 
  }
  else if(type.indexOf('error')!=-1){
    $(elmnt).removeClass('alert-success');  
    $(elmnt).addClass('alert-danger');
    $(elmnt+" i").removeClass('fa-check');  
    $(elmnt+" i").addClass('fa-ban');

  }
}
function loading(toggle){
	if(toggle==true){$("#loading-overlay").fadeIn(200);}
	else{$("#loading-overlay").fadeOut(200);}
}

$(document.body).on('click', '.js-post' ,function(){
  var settings = {
      "responseType":"none",
  }
  var confirmation = $(this).attr('data-confirm');
  var url = $(this).attr('data-url');
  var data = $.parseJSON($(this).attr('data-post')); data = arrayToURL(data);

  if((typeof confirmation !=="undefined" && confirm(confirmation)) || typeof confirmation === "undefined"){

    loading(true);
  	$.ajax({type: "POST", url: url,data:data}).done(function(result){
          result = $.parseJSON(result);
          if(typeof result.callFunction!="undefined" && result.callFunction!=""){
            window[result.callFunction](result);
          }

          if(typeof result.url!="undefined" && result.url!=""){
              if(result.url=="self")
                setTimeout(function(){location.reload();},1000);
              else if(result.url!="")
                window.location.href = result.url;
          }
  	});
  }
	loading(false);	
});


function submit_form(form){
     var url = $(form).attr('action');
     loading(true);
     $(form+" .form-msg").removeClass('shake');
     $(form+" .form-msg").removeClass('bounce');
     $.ajax({type:"POST", data: $(form).serialize(), url: url}).done(function(result){
      
     	  $(form+" .form-msg").show(0);
       	var result = $.parseJSON(result);
        alert_div(form+' .form-msg',result.status);
        if(result.status.indexOf('-reset')>=0){
            $(form+" .form-reset").val('');
        }
        if(result.status.indexOf('error')>=0){
 			$(form+" .form-msg").addClass('shake');
        }
        else{
        	$(form+" .form-msg").addClass('bounce');
        }
        if(typeof result.url!="undefined" && result.url!=""){
            if(result.url=="self")
              setTimeout(function(){location.reload();},1000);
            else if(result.url!="")
              window.location.href = result.url;
        }
        else
          $("#loading-overlay").fadeOut(200);
        if(typeof result.callFunction!="undefined" && result.callFunction!=""){

          window[result.callFunction](result);
        }
        $(form+" .form-msg span").html(result.message);
        loading(false);
     });
}

function convert_to_currency_format(value){
  value = number_format(value,0,',','.');
  return value;
}

/* ---------------------------------- Outsource Scripts ------------------------------ */

function arrayToURL(array) {
  var pairs = [];
  for (var key in array)
    if (array.hasOwnProperty(key))

      pairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(array[key]));
  return pairs.join('&');
}


function number_format(number, decimals, dec_point, thousands_sep) {
  number = (number + '')
    .replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
      .join('0');
  }
  return s.join(dec);
}