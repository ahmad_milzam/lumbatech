function bankDeleted(response){
	if(response.status=="success"){
		$("#account-row-"+response.id).remove();
	}
}

function add_bank(id){
	$("#add-bank-form-"+id).ajaxForm({ 
		success: function(responseText){
			if(id>0){
				$("#account-row-"+id).html(responseText);
			}
			else{
				$(responseText).insertBefore('#account-row-0');
				$(".add-bank-txt").val('');
			}
		} 
	}).submit(); 
	return false;

}

function edit_bank(id){
	var base_url = $("#base_url").attr('href');
	$.ajax({type: "POST", url: base_url+"admin/dashboard/editBank",data: "id="+id}).done(function(result){
		$("#account-row-"+id).html(result);
	});
}


function change_password(){
	$("#change-password-msg").html('');
	$("#change-password-form").ajaxForm({ 
		target: '#change-password-msg',
		success: function(responseText){
			if(responseText=='Password successfully changed'){
				$('.change-password-txt').val('');
			}
		} 
	}).submit(); 
	return false;

}
