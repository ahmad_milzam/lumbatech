// function changeToForm(elmnt){
// 	var id = elmnt.parent().attr('data-id');
// 	var name = elmnt.attr('data-name');
// 	var value = elmnt.attr('data-value');
// 	var url = elmnt.parent().attr('data-url');
// 	var content = "<form action='"+url+"' method='post' class='form-"+id+"'>"+
// 					"<input type='hidden' value='"+id+"' name='id'>"+
// 					"<input type='text' value='"+value+"' name='"+name+"'>"+
// 					"<button class='btn' onclick=\"submit_form($('.form-"+id+"')); return false;\"></button>"+
// 				   "</form>";
// 	elmnt.html(content);
// }
function variantImageChanged(result){
	var base_url = $("#base_url").attr('href');
	var img_url = base_url+"data/product/tmb/"+result.message;
	$("#variant-img-"+result.id).attr('src',img_url);
	$("#product-img-modal").modal('hide');
}
function changeVariantImg(id){
	$(".variant-image-variant-id").val(id);
	$("#product-img-modal").modal('show');
}
function editVariantItem(id,product_name,variant_string){

	var price = $("#variant-item-price-"+id).html();
	price = unconvert_currency(price.replace('IDR ',''));
	var weight = $("#variant-item-weight-"+id).html();
	var stock = $("#variant-item-stock-"+id).html();

	$("#edit-variant-form input[name='variant-id']").val(id);
	$("#edit-variant-form input[name='price']").val(price);
	$("#edit-variant-form input[name='weight']").val(weight);
	$("#edit-variant-form input[name='stock']").val(stock);
	$("#edit-variant-product-name").html(product_name);
	$("#edit-variant-variant-string").html(variant_string);
	$("#edit-variant-modal").modal('show');
}

function variantEdited(result){
	$("#variant-item-price-"+result.data.VariantID).html('IDR '+convert_to_currency_format(result.data.VariantPrice));
	$("#variant-item-stock-"+result.data.VariantID).html(result.data.VariantStock);
	$("#variant-item-weight-"+result.data.VariantID).html(result.data.VariantWeight);
	$("#edit-variant-modal").modal('hide');
}

function uploadDropzone(){
	var productId = $("#product-id").val();
	myDropzone.on("sending", function(file, xhr, data) {
        data.append("product-id", productId);
    });
	myDropzone.processQueue();
	myDropzone.on("success", function(file, serverFileName) {
		appendProductImg(serverFileName, productId);
	});
}
function appendProductImg(filename,productId){
	var base_url = $("#base_url").attr('href');
	var imageId = filename.substr(0, filename.indexOf('-'));
    var content = '<div class="col-sm-2 col-xs-3 product-img-item" id="product-img-'+imageId+'">'+
        '<img src="'+base_url+'data/product/tmb/'+filename+'" data-url="'+base_url+'data/product/'+filename+'" class="other-product-img img-responsive">'+
        '<div class="product-img-button">'+
            '<div class="button-container">'+
                '<a class="btn btn-xs" onclick="set_as_primary('+imageId+','+productId+');"><i class="fa fa-star"></i></a> '+
                '<a class="btn btn-xs" onclick="delete_product_img('+imageId+');"><i class="fa fa-trash-o"></i></a>'+
            '</div>'+
        '</div>'+
    '</div>';
    $("#product-img-container").append(content);
}
function add_product(type){
    $("#form-msg").fadeIn(200);
    $("#form-msg").removeClass('shake');
    $("#form-msg").removeClass('bounce');
	$("#add-product-form").ajaxForm({ 
		success: function(result){
          	var result = $.parseJSON(result);
            alert_div('#form-msg',result.status);
			if(result.status=='success'){
				if(type=='Add Product'){
					$("#add-product-form input[type='text']").val('');
					$("#add-product-form textarea").val('');
	     			$("#dropzone-product-id").val(result.id);
			 		myDropzone.on("sending", function(file, xhr, data) {
			            data.append("product-id", result.id);
			        });
			     	myDropzone.processQueue();
				}
     			$("#form-msg").addClass('bounce');
			}
			else{
     			$("#form-msg").addClass('shake');
			}
          	$("#form-msg span").html(result.message);
		} 
	}).submit(); 
	return false;
}
function edit_product(){
	$("#form-msg").fadeIn(200);
    $("#form-msg").removeClass('shake');
    $("#form-msg").removeClass('bounce');
	$("#edit-product-form").ajaxForm({ 
		success: function(result){
          	var result = $.parseJSON(result);
            alert_div('#form-msg',result.status);
			if(result.status=='success'){
     			$("#form-msg").addClass('bounce');
			}
			else{
     			$("#form-msg").addClass('shake');
			}
          	$("#form-msg span").html(result.message);
		} 
	}).submit(); 
	return false;
}
function add_product_detail(){
	$("#add-product-detail-form").ajaxForm({ 
		//target: '#add-product-msg', 
		success: function(responseText){
			$(".add-product-detail-form").val('');
			$(responseText).insertBefore('#add-product-detail-form');
		} 
	}).submit(); 
	return false;
}

function add_product_img(){
	$("#add-product-img-form").ajaxForm({ 
		//target: '#add-product-msg', 
		success: function(result){
          	var result = $.parseJSON(result);
          	if(result.status=='success'){
          		$("#product-img-container").append(result.message);
          	}
			$("#product-img").val('');
		} 
	}).submit(); 
	return false;
}

function delete_product_img(id){
	if(confirm("Do you want to delete this image ?")){
		var base_url = $("#base_url").attr('href');
		$.ajax({type: "POST", url: base_url+"admin/product/delete_product_img",data: "id="+id}).done(function(result){
			$("#product-img-"+id).remove();
		});
	}
}
function delete_product_detail(id){
	if(confirm("Are you sure want to delete this product detail?")){
		var base_url = $("#base_url").attr('href');
		$.ajax({type: "POST", url: base_url+"admin/doDeleteProductDetail",data: "product-detail-id="+id}).done(function(result){
			if(result=="Success"){
				$("#product-detail-"+id).remove();
			}
		});
	}
}

function set_as_primary(id,productid){
	var base_url = $("#base_url").attr('href');
	$.ajax({type: "POST", url: base_url+"admin/product/primary_product_img",data: "id="+id+"&productid="+productid}).done(function(result){
		var src = $("#product-img-"+id+' img').attr('data-url');
		$("#product-detail-image img").attr('src',src);
	});
}

function edit_product_detail(id){
	var base_url = $("#base_url").attr('href');
	$.ajax({type: "POST", url: base_url+"admin/editProductDetail",data: "product-id="+id}).done(function(result){
		$("#edit-product-detail").html(result);
		$("#edit-product-detail-box").slideDown(200);
	});

}

function cancel_edit_product_detail(){
	$("#edit-product-detail").html('');
	$("#edit-product-detail-box").slideUp(200);

}

function save_edit_product_detail(){
	$("#edit-product-detail-form").ajaxForm({ 
		//target: '#add-product-msg', 
		success: function(responseText){
			if(responseText=="Success"){
				location.reload();
			}
		} 
	}).submit(); 
	return false;	
}

function show_email_editor(elmnt){

	if(elmnt.val()=='2' || elmnt.val()=='3' ){
		$(".voucher-only-form").show();
	}
	else{
		$(".voucher-only-form").hide();	
	}
}

function delete_product(id){
	if(confirm("All data related to this product will be deleted, you want to delete this product?")){
		var base_url = $("#base_url").attr('href');
		$.ajax({type: "POST", url: base_url+"admin/doDeleteProduct",data: "product-id="+id}).done(function(result){
          	var result = $.parseJSON(result);
			if(result.status=="success"){
				$("#product-list-"+id).remove();
			}
		});
	}
}
function show_product(id){
	var base_url = $("#base_url").attr('href');
	$.ajax({type: "POST", url: base_url+"admin/doShowProduct",data: "product-id="+id}).done(function(result){
		if(result=="On"){
			$("#show-product-"+id).removeClass('btn-default');
			$("#show-product-"+id).addClass('btn-success');
		}else{
			$("#show-product-"+id).removeClass('btn-success');
			$("#show-product-"+id).addClass('btn-default');
		}
		$("#show-product-"+id).attr('title','Show Product : '+result);
	});
}
function sale_product(id){
	var base_url = $("#base_url").attr('href');
	$.ajax({type: "POST", url: base_url+"admin/doSaleProduct",data: "product-id="+id}).done(function(result){
		if(result=="On"){
			$("#sale-"+id).removeClass('btn-default');
			$("#sale-"+id).addClass('btn-danger');
		}else{
			$("#sale-"+id).removeClass('btn-danger');
			$("#sale-"+id).addClass('btn-default');
		}
	});
}
function process_transaction(id,type){
	if(confirm("Process this transaction?")){
		var base_url = $("#base_url").attr('href');
		$.ajax({type: "POST", url: base_url+"admin/processTransaction",data: "transaction-id="+id+"&type="+type}).done(function(result){
			$("#status-name").html(result);
		});
	}
}
function limited_stock(){
	if($("#limited-stock").is(':checked')){
		$("#product-stock").show(0);
	}
	else{
		$("#product-stock").hide(0);	
	}
}
function update_website_info(){
    $("#form-msg").fadeIn(200);
    $("#form-msg").removeClass('shake');
    $("#form-msg").removeClass('bounce');
	$("#website-setting-form").ajaxForm({ 
		success: function(result){
          	var result = $.parseJSON(result);
            alert_div('#form-msg',result.status);
			if(result.status=='success'){
     			$("#form-msg").addClass('bounce');
			}
			else{
     			$("#form-msg").addClass('shake');
			}
          	$("#form-msg span").html(result.message);
		} 
	}).submit(); 
	return false;
}

function submit_genre(){
     $("#form-msg").removeClass('shake');
     $("#form-msg").removeClass('bounce');
     $("#genre-form").ajaxForm({
          success: function(result){
               $("#form-msg").fadeIn(300);
               var result = $.parseJSON(result);
               alert_div('#form-msg',result.status);
               if(result.status=="error"){
                    $("#form-msg").addClass('shake');
               }
               else if(result.status=="success"){
                    $("#form-msg").addClass('bounce');
                    setTimeout(function(){
                         location.reload();
                    },0);
               }
               $("#form-msg span").html(result.message);

          }
     }).submit();
     return false;
}

function product_option(value){
     //alert('awl');
     $(".option-stock").hide(0);
     if(value=='no-option'){
          $("#no-option-stock").show(0);
     }
     else if(value=='size-option'){
          $("#size-option-stock").show(0);
     }
}

function add_size(){
	var content = '<div class="row"><div class="col-md-6"><input type="text" name="stockname[]" class="form-control" placeholder="Option Name"></div><div class="col-md-6"><input type="text" name="stock[]" class="form-control" placeholder="Stock (empty value means unlimited stock)"></div></div>';
	$("#size-option-stock-container").append(content);
}
function add_testi_img(){
	$("#add-testi-img-form").ajaxForm({ 
		success: function(result){
			$("#slider-file-img").val('');
          	var result = $.parseJSON(result);
          	if(result.status=='success'){
				$("#testi-image-container").prepend(result.message);
          	}
		} 
	}).submit(); 
	return false;

}

function delete_testi(id){
	if(confirm("Are you sure want to delete this testimonial image?")){
		var base_url = $("#base_url").attr('href');
		$.ajax({type: "POST", url: base_url+"admin/doDeleteTesti",data: "id="+id}).done(function(result){
			if(result=="success"){
				$("#testi-img-"+id).remove();
			}

		});
	}
}

$(document).ready(function(){
	initTagsInput();
	$(".add-variant-btn").click(function(){
		$("#variants-container").toggleClass('active');
		$(".add-variant-btn").show(0);
		$(this).hide(0);
		activateVariant();
	});

	$("#add-variant").click(function(){
		var total_variant = $(".variant").length;
		if(total_variant<3){
			var content = '<div class="row variant"><div class="col-sm-4"><input type="text" class="form-control mb-10" placeholder="Variant Name (ex: Color, Size, Material)" name="variant-name[]"></div><div class="col-sm-7"><input type="text" class="form-control mb-10 tags-input" name="variant-value[]"></div><div class="col-sm-1"><div class="btn btn-default delete-variant-btn" onclick="deleteVariant($(this));"><i class="fa fa-trash-o"></i></div></div></div>';
			if(total_variant==2){
				$(this).hide(0);
			}
			$("#variants-item").append(content);
		}
		if(total_variant>0){
			$(".delete-variant-btn").show(0);
		}
		initTagsInput();
	});
	if($("#dropzone").length>0){
		$("#dropzone").dropzone({
		  url: $("#base_url").attr('href')+"admin/product/submit_product_img",
		  method: "post",
		  acceptedFiles: "image/*",
		  addRemoveLinks: true,
			removedfile: function(file) {
		    var name = file.name;
			var _ref;
			return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
		  },
		  autoProcessQueue: false,
		  init: function() {
		    myDropzone = this; // closure

		    $("#upload").click(function(){
		      myDropzone.processQueue(); // Tell Dropzone to process all queued files.
		    });

		    // You might want to show the submit button only when 
		    // files are dropped here:
		    
			this.on("success", function(file, serverFileName) {
		  		myDropzone.removeFile(file);
			});

		  }
		});
	}
});

function deleteVariant(element){
	var total_variant = $(".variant").length;
	if(total_variant>1){
		element.parent().parent().remove();
	}
	if(total_variant<=2){
		$(".delete-variant-btn").hide(0);
	}
	$("#add-variant").show();
}

function activateVariant(){
	if($("#cancel-variant").is(":visible")){
		$("#variants-flag").val('on');
	}
	else{
		$("#variants-flag").val('off');
	}

}
function initTagsInput(){
	if($(".tags-input").length>0){
		$(".tags-input").tagsInput();
		$(".tags-input").removeClass('tags-input');
	}
}

function generateVariants(){
	var value = new Array();
	var variant = 0;
	var content = "";
	$(".tagsinput").each(function(index){
		value[variant] = "";
		$(this).children('span').each(function(i){
			var tag=$(this).html().substring($(this).html().lastIndexOf("<span>")+6,$(this).html().lastIndexOf("&nbsp;&nbsp;</span>"));
			value[variant] += tag+",";

		});
		variant++;
	});

	// for(var i = 0; i<variant ; i++){
	// 	for(var x = 0; x<value[variant].split(',').length; x++){
	// 		content += "<tr>";
	// 		content += "<td valign='middle'>"+tag+"</td>";
	// 		content += "<td><input type='text' class='form-control'></td>";
	// 		content += "<td><input type='text' class='form-control'></td>";
	// 		content += "</tr>";
	// 	}
	// }

	if(typeof value[0]!="undefined"){

		var tags_1 = value[0].split(',');

		for(var x = 0; x<tags_1.length-1; x++){
			if(typeof value[1]!="undefined" && value[1]!=""){
				var tags_2 = value[1].split(',');
				for(var y = 0; y<tags_2.length-1; y++){
					if(typeof value[2]!="undefined" && value[2]!=""){
						var tags_3 = value[2].split(',');
						for(var z = 0; z<tags_3.length-1; z++){
							content += generateVariantContent(tags_1[x],tags_2[y],tags_3[z]);
						}
					}
					else{
						content += generateVariantContent(tags_1[x],tags_2[y],'');
					}
				}
			}else{
				content += generateVariantContent(tags_1[x],'','');
			}
		}
	}

	var variant_table = '<table width="100%"><thead><tr><td>Variant</td><td>Price</td><td>Stock</td><td>Weight</td></tr></thead><tbody>'+content+'</tbody></table>';
	$("#variant-details").html(variant_table);
	
}

function generateVariantContent(x,y,z){
	var content = '';
	content += "<tr>";
	content += "<td valign='middle'>"+x+" "+y+" "+z+"</td>";
	content += "<td>";
	content += "<input type='text' name='variant-price[]' class='form-control'>";
	content += "<input type='hidden' name='variant-1[]' value='"+x+"'>";
	content += "<input type='hidden' name='variant-2[]' value='"+y+"'>";
	content += "<input type='hidden' name='variant-3[]' value='"+z+"'>";
	content += "</td>";
	content += "<td><input type='number' name='variant-stock[]' class='form-control' value='0'></td>";
	content += "<td><input type='number' name='variant-weight[]' class='form-control' value='0'></td>";
	content += "</tr>";
	return content;
}