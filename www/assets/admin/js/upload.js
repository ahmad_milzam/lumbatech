
function do_upload(){
	$("#upload-image-form").ajaxForm({ 
		success: function(result){
          	var result = $.parseJSON(result);
          	if(result.status=="success")
				$("#gallery-container").prepend(result.message);
			$("#file").val('');
		} 
	}).submit(); 
	return false;
}
function delete_image(id){
	var base_url = $("#base_url").attr('href');
	if(confirm("Are you sure want to delete this picture?")){
		$.ajax({url: base_url+"admin/doDeleteImage", type: "post", data: "id="+id}).done(function(result){
			if(result=="success"){
				$("#gallery-img-"+id).remove();
			}
		});
		
	}
}