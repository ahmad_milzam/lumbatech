function message_popup(type){
    var reply;
    if(type==1){
        reply = prompt("Insert link URL","");
        if(reply!="" && reply!=null)
            reply = "[LINK="+reply+"][/LINK]";
    }
    else if(type==2){
        reply = prompt("Insert image URL","");
        if(reply!="" && reply!=null)
            reply = "[IMAGE]"+reply+"[/IMAGE]";
    }
    else if(type==3){
        reply = prompt("Insert Vimeo ID","");
        if(reply!="" && reply!=null)
            reply = "[VIMEO]"+reply+"[/VIMEO]";
    }
    else if(type==4){
        reply = prompt("Insert Youtube ID","");
        if(reply!="" && reply!=null)
            reply = "[YOUTUBE]"+reply+"[/YOUTUBE]";
    }
    else if(type==5){
        reply = prompt("Insert color / color code","");
        if(reply!="" && reply!=null)
            reply = "[COLOR="+reply+"][/COLOR]";
    }
    else if(type==6){
        reply = prompt("Font size","");
        if(reply!="" && reply!=null)
            reply = "[SIZE="+reply+"][/SIZE]";
    }
    if(reply==null)
        reply = "";
    return reply;
}


function setSelectionRange(input, selectionStart, selectionEnd) {
  if (input.setSelectionRange) {
    input.focus();
    input.setSelectionRange(selectionStart, selectionEnd);
  }
  else if (input.createTextRange) {
    var range = input.createTextRange();
    range.collapse(true);
    range.moveEnd('character', selectionEnd);
    range.moveStart('character', selectionStart);
    range.select();
  }
}

function setCaretToPos (input, pos) {
    setSelectionRange(input, pos, pos);
    $(input).scrollTop(input.scrollHeight+100);
}

function insertAtCaret(areaId,text,mid) {
    var txtarea = document.getElementById(areaId);
    var scrollPos = txtarea.scrollTop;
    var strPos = 0;
    var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ? 
        "ff" : (document.selection ? "ie" : false ) );
    if (br == "ie") { 
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -txtarea.value.length);
        strPos = range.text.length;
    }
    else if (br == "ff") strPos = txtarea.selectionStart;

    var front = (txtarea.value).substring(0,strPos);  
    var back = (txtarea.value).substring(strPos,txtarea.value.length); 
    txtarea.value=front+text+back;
    strPos = strPos + text.length;
    if (br == "ie") { 
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -txtarea.value.length);
        range.moveStart ('character', strPos);
        range.moveEnd ('character', 0);
        range.select();

    }
    else if (br == "ff") {
        txtarea.selectionStart = strPos;
        if(mid!=0)
           txtarea.selectionEnd = strPos-mid;
        else
            txtarea.selectionEnd = strPos;
        txtarea.focus();

    }
    txtarea.scrollTop = scrollPos;
}