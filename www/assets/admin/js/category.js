function add_category(){
     $("#add-category-form").ajaxForm({
          success: function(result){
               var result = $.parseJSON(result);
               if(result.status=="error"){   
                    $("#add-category-msg").html(result.message);
               }
               else{   
                    location.reload();
               }
          }
     }).submit();
     return false;
}

function delete_category(id){
     if(confirm("Delete this category?")){
          var base_url = $("#base_url").attr('href');
          $.ajax({type: "POST", url: base_url+"admin/doDeleteCategory",data: "id="+id}).done(function(result){
               alert(result);
               var result = $.parseJSON(result);
               if(result.status=="success"){
               }
          });
     }
}