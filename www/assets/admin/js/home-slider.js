
function add_slider_img(){
	loading(true);
	$("#add-slider-img-form").ajaxForm({ 
		success: function(result){
			loading(false);
			$("#slider-file-img").val('');
          	var result = $.parseJSON(result);
          	if(result.status=='success'){
				$("#slider-image-container tbody").prepend(result.message);
          	}
		} 
	}).submit(); 
	return false;

}

function delete_slider_img(id){
	if(confirm("Are you sure want to delete this slider image?")){
		var base_url = $("#base_url").attr('href');
		$.ajax({type: "POST", url: base_url+"admin/doDeleteSliderImage",data: "id="+id}).done(function(result){
			if(result=="success"){
				$("#slider-img-"+id).remove();
			}

		});
	}
}