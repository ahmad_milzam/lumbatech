
(function (global, factory) {
  if (typeof define === 'function' && define.amd) {
    define([], factory);
  }
  else if (typeof exports === 'object') {
    module.exports = factory();
  }
  else {
    global.whichTransitionEnd = factory();
  }
}(this, function() {

  /**
   * Determine the name of the current browser’s css transition end event,
   * vendor prefixed if necessary.
   *
   * @return {String} Name of the transition end event
   */

  function whichTransitionEnd() {
    var el = document.createElement('div'),
        transition,

        // 1. Safari 6, Android Browser
        // 2. only for FF < 15
        // 3. IE10, Opera, Chrome, FF 15+, Safari 7+

        eventNames = {
          'WebkitTransition': 'webkitTransitionEnd', // 1
          'MozTransition': 'transitionend',          // 2
          'transition': 'transitionend'              // 3
        };

    for (transition in eventNames) {
      if (el.style[transition] !== undefined) {
        return eventNames[transition];
      } else{
        return false;
      }
    }
  }

  /**
   * Expose the function.
   */

  return whichTransitionEnd;

}));
