(function(document, $){

  var topbar             = $('.js-topbar'),
      topbarToggle       = $('.js-topbarToggle');

  topbarToggle.on('click', function(event){
    var $this = $(this);
    $this.closest('.js-topbar').toggleClass('topbar-open');
  });

})(document, jQuery);
