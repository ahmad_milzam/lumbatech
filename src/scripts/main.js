(function($){

  var viewport = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

  var $equalColumn = $('.js-equalColumn');
  if($equalColumn.length){
    $equalColumn.matchHeight({
      byRow: true
    });
  }

  var $slider = $('#js-slider');
  if($slider.length){

    $slider.slick({
      autoplay: true,
      autoplaySpeed: 3000,
      dots: true,
      prevArrow: '.slide-nav--prev',
      nextArrow: '.slide-nav--next',
      infinite: true,
      speed: 1000,
      fade: true,
      cssEase: 'ease'
    });

  }

  var $sidebar = $('#js-sidebar');
  var $toggleSubmenu = $('.js-toggleSubmenu');
  if($sidebar.length){

    // if(viewport > 1000 && !isMobile.any){
    //   $sidebar.menuAim({
    //     activate: function(row) {
    //       $(row).addClass('is-hover');
    //     },
    //     deactivate: function(row) {
    //       $(row).removeClass('is-hover');
    //     },
    //     exitMenu: function() {
    //       $sidebar.find('.is-hover').removeClass('is-hover');
    //       return true;
    //     },
    //     submenuSelector: ".has-submenu",
    //   });
    // }

    $toggleSubmenu.on('click', function(event) {
      event.preventDefault();
      /* Act on the event */
      var $this = $(this);
      var $root = $this.closest('.sidebar__list');
      var $parent = $this.closest('.sidebar__item');
      var classname = 'is-open';

      // if(viewport > 720){
      //   classname = 'is-hover';
      // }
      // console.log(classname);


      if($parent.hasClass(classname)){
        $parent.removeClass(classname);
        // console.log('parent has class, remove '+classname);
      } else{
        $root.children('.'+classname).removeClass(classname);
        $parent.addClass(classname);
      }
    });

    /* close submenu if user click anywhere outside the sidebar link */
    $(document).on('click', function(event){
      if( !$(event.target).is('.js-toggleSubmenu') ) {
        $sidebar.children('.is-open').removeClass('is-open');
      }
    });

  }

  var $formContact = $('#js-formContact');
  if($formContact.length){
    console.log('form exist');
    $formContact.on('submit', function(event) {
      event.preventDefault();
      console.log('form submit');

      ajaxSubmitForm($formContact);

      // if($formContact instanceof jQuery){
      //   console.log('ini jquery object');
      // } else {
      //   console.log('bukan jquery object');
      // }
      return false;
    });
  }

})(jQuery);