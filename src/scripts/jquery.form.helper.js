/**
 * Clear form input after submit
 * @return {[type]} [description]
 */
$.fn.clearForm = function() {
  return this.each(function() {
    var type = this.type, tag = this.tagName.toLowerCase();
    if (tag == 'form')
      return $(':input',this).clearForm();
    if (type == 'text' || type == 'password' || type == 'email' || type == 'tel' || tag == 'textarea')
      this.value = '';
    else if (type == 'checkbox' || type == 'radio')
      this.checked = false;
    else if (tag == 'select')
      this.selectedIndex = -1;
  });
};

/**
 * Ajax Submit Helper
 */
function ajaxSubmitForm(form){
    console.log(form instanceof jQuery);

    var $form = (form instanceof jQuery) ? form : $(form),
        $submitBtn = $form.find('[type=submit]'),
        loadingState = 'Please wait..',
        defaultState = $submitBtn.html();

    $submitBtn.html(loadingState);

    $.ajax({
        url: $form.attr('action'),
        type: $form.attr('method'),
        data: $form.serialize(),
        dataType: 'json'
    })
    .done(function(response, textStatus, jqXHR){
        // response = {
        //     status: 'error | success',
        //     message: 'string',
        //     url: ''
        // }
        if(response.status === 'error'){
            AlertBox.show(response.message, {
                alertType: 'error'
            });
        } else{
            AlertBox.show(response.message, {
                alertType: 'success'
            });
            $form.clearForm();
        }
        if(typeof response.callFunction!="undefined" && response.callFunction!=""){
          window[response.callFunction](response);
        }
        if(response.url && response.url =="self"){
            setTimeout(function(){
                window.location.reload();
            },2500);
        } else if (response.url && response.url != "self") {
            setTimeout(function(){
                window.location.href = response.url;
            },2500);
        }
    })
    .fail(function( jqXHR, textStatus, errorThrown ){
        alert('An error occured\n'+errorThrown);
    })
    .always(function(){
        $submitBtn.html(defaultState);
    });

}