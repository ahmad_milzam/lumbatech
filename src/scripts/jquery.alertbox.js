var AlertBox = (function($){

  "use-strict";
  var alertbox = {},
      settings = {
        alertContainer: '#js-alertContainer',
        alertClass: 'alert',
        alertType: '',
        alertHeaderClass: 'alert__heading',
        alertHeaderText: 'Heads up!',
        alertBodyClass: 'alert__content',
        alertCloseClass: 'alert__close',
        alertEntranceClass: 'alert-in',
        alertExitClass: 'alert-out',
        alertCallback: null
      },
      template,
      transitionEvent = whichTransitionEnd(),
      animationEvent = whichAnimationEnd();

  alertbox.show = function(msg, options){
    if(typeof options == 'undefined'){
      var options = {};
    }
    $.extend( settings, options );
    template = '';

    var type = settings.alertType ? 'alert--'+settings.alertType : '';

    template += '<div class="alert '+type+' '+settings.alertEntranceClass+'  js-alert">';
    if (settings.alertHeaderText) {
      template += '<h4 class="'+settings.alertHeaderClass+'">'+settings.alertHeaderText+'</h4>';
    }
    template += '<div class="'+settings.alertBodyClass+'">'+msg+'</div>';
    template += '<a href="#" class="'+settings.alertCloseClass+' js-alert-close">&times;</a>';
    template += '</div>';

    $(settings.alertContainer).html(template);
  }

  return alertbox;

}(jQuery));