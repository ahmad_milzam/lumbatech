
(function (global, factory) {
  if (typeof define === 'function' && define.amd) {
    define([], factory);
  }
  else if (typeof exports === 'object') {
    module.exports = factory();
  }
  else {
    global.whichAnimationEnd = factory();
  }
}(this, function() {

  /**
   * Determine the name of the current browser’s css transition end event,
   * vendor prefixed if necessary.
   *
   * @return {String} Name of the transition end event
   */

  function whichAnimationEnd() {
    var el = document.createElement('div'),
        animation,

        // 1. IE10, Opera 20+, Chrome, FF 15+, Safari 7+
        // 2. Opera < 20
        // 3. Safari 6, Android Browser

        eventNames = {
          "animation"      : "animationend",      // 1
          "OAnimation"     : "oAnimationEnd",     // 2
          "WebkitAnimation": "webkitAnimationEnd" // 3
        };

    for (animation in eventNames) {
      if (el.style[animation] !== undefined) {
        return eventNames[animation];
      } else{
        return false;
      }
    }
  }

  /**
   * Expose the function.
   */

  return whichAnimationEnd;

}));
